﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class News : System.Web.UI.Page
{
    private const int NB_PER_PAGE = 6;

    protected void Page_Load(object sender, EventArgs e)
    {
        bool showOGtags = Request.UserAgent.Contains("facebookexternalhit") || Request.UserAgent.Contains("Pinterest") || Request.UserAgent.Contains("Twitterbot") || Request.Browser.Crawler;
        ((ITmgMasterPage)Master).ShowOGtags = showOGtags;

        breadcrumbs1.AddLevel("News", "/News/");

        SqlDataReader dr;

        int id;
        if (!int.TryParse(Convert.ToString(Page.RouteData.Values["id"]), out id))
        {
            id = -1;
        }

        dr = DB.DbFunctions.GetNews(id);

        if (dr != null)
        {
            if (dr.HasRows)
            {
                int start = 0;
                if (int.TryParse(Request.QueryString["s"], out start) && start >= NB_PER_PAGE)
                {
                    lnkPrev.HRef = "/News/?s=" + (start - NB_PER_PAGE).ToString();
                    lnkPrev.Visible = true;
                }

                lnkNext.HRef = "/News/?s=" + (start + NB_PER_PAGE).ToString();
                while (start-- > 0 && dr.Read()) ;

                int i = 0;
                StringBuilder sb = new StringBuilder();
                bool first = true;
                while (dr.Read() && i++ < NB_PER_PAGE)
                {
                    //string link = String.Format("<a href=\"{0}\" target=\"_blank\">", System.Configuration.ConfigurationManager.AppSettings["baseurl"] + "news/" + Convert.ToString(dr["ID"]) + "/" +Convert.ToString(dr["Title"]));
                    string newsurl = "/News/" + dr["ID"] + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                    string imageurl = System.Configuration.ConfigurationManager.AppSettings["baseimagesnewsurl"] + Convert.ToString(dr["Thumbnail"]);
                    string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 220).Replace("\"", " ").Trim();
                    string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Title"])), 220).Replace("\"", " ").Trim();

                    sb.Append("<div class=\"new\">");
                    sb.AppendFormat("<div class=\"img\"><a href=\"{3}\"><img alt=\"{2}\" src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagesnewsurl"], Functions.ConvertToString(dr["Thumbnail"]), Convert.ToString(dr["Title"]), newsurl);
                    sb.Append("<div class=\"description\">");
					sb.AppendFormat("<h2 class=\"title\"><a href=\"{1}\">{0}</a></h2>", Functions.ConvertToString(dr["Title"]), newsurl);
					sb.Append(Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 300));
					sb.AppendFormat("<div class=\"button\"><a href=\"{0}\">Read More</a></div>", newsurl);
					sb.Append("<div id=\"socialst\">");
                    sb.AppendFormat("    <div class=\"st_facebook_hcount item\" st_url=\"{0}{1}\" st_image=\"{2}\" st_title=\"{3}\" st_summary=\"{4}\" displayText=\"Facebook\"></div>", System.Configuration.ConfigurationManager.AppSettings["baseurl"], newsurl, imageurl, title, description);
                    sb.AppendFormat("    <div class=\"st_twitter_hcount item\" st_url=\"{0}{1}\" st_image=\"{2}\" st_title=\"{3}\" st_summary=\"{4}\" displayText=\"Tweet\"></div>", System.Configuration.ConfigurationManager.AppSettings["baseurl"], newsurl, imageurl, title, description);
                    sb.AppendFormat("    <div class=\"st_pinterest_hcount item\" st_url=\"{0}{1}\" st_image=\"{2}\" st_title=\"{3}\" st_summary=\"{4}\" displayText=\"Pinterest\"></div>", System.Configuration.ConfigurationManager.AppSettings["baseurl"], newsurl, imageurl, title, description);
                    sb.AppendFormat("    <div class=\"st_email_hcount item\" st_url=\"{0}{1}\" st_image=\"{2}\" st_title=\"{3}\" st_summary=\"{4}\" displayText=\"Email\"></div>", System.Configuration.ConfigurationManager.AppSettings["baseurl"], newsurl, imageurl, title, description);
                    sb.Append("</div>");
                    sb.Append("</div>");

                    sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                    //if (i % 3 == 0)
                    //{
                    //    if (Request.Browser.IsMobileDevice)
                    //    {
                    //        sb.Append("<div class=\"ad\">");
                    //        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- HMG - Body --><ins class=\"adsbygoogle\" style=\"display:inline-block;width:98%;height:50px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"9580178383\" data-ad-format=\"auto\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                    //        sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                    //    }
                    //}

                    if (i == 1 && showOGtags)
                    {
                        ((ITmgMasterPage)Master).PageLogo = System.Configuration.ConfigurationManager.AppSettings["baseimagesnewsurl"] + Functions.ConvertToString(dr["Thumbnail"]);
                        ((ITmgMasterPage)Master).PageTitle = Convert.ToString(dr["Title"]);
                        ((ITmgMasterPage)Master).PageURL = newsurl;
                        ((ITmgMasterPage)Master).PageType = "article";
                        ((ITmgMasterPage)Master).PageDescription = Convert.ToString(dr["Text"]);
                    }
                }

                //if (i < 3)
                //{
                //    if (Request.Browser.IsMobileDevice)
                //    {
                //        sb.Append("<div class=\"ad\">");
                //        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- HMG - Body --><ins class=\"adsbygoogle\" style=\"display:inline-block;width:98%;height:50px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"9580178383\" data-ad-format=\"auto\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                //        sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                //    }
                //}

                litCoupons.Text = sb.ToString();

                if (i > NB_PER_PAGE)
                    lnkNext.Visible = true;
            }
            dr.Close();
            dr.Dispose();
        }
    }
}