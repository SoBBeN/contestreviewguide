﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Photos.aspx.cs" Inherits="Photos" %>

<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>
<%@ Register TagPrefix="fbc" TagName="FbComments" Src="~/UserControls/FbComments.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.8.3/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.9.2/jquery-ui.min.js"></script>
    <link href="http://code.jquery.com/ui/1.9.2/themes/start/jquery-ui.css" rel="stylesheet" type="text/css" />
    
    <link href="/css/photo.css" rel="stylesheet" />
    <script type="text/javascript" src="/js/social.js"></script>
    <script type="text/javascript">
        /* Next Prev on Main Page */
        function Next() {
            TMG.IService.GetPhotos(document.getElementById('firstid').value, 1, document.getElementById("typeid").value, UpdatePage);
        }
        function Previous() {
            TMG.IService.GetPhotos(document.getElementById('firstid').value, -1, document.getElementById("typeid").value, UpdatePage);
        }
        function UpdatePage(photos) {
            console.log(photos);
            document.getElementById('photoslist').innerHTML = photos;
        }

        /* Open and Next Prev in Dialog */
        function OpenPhoto(id) {
            $("#dlginspiration img").attr("src", $("#img" + id).attr("src"));
            TMG.IService.GetPhoto(id, 0, document.getElementById("typeid").value, UpdatePhoto);
            $("#dlgPhoto").dialog("open");
        }
        function NextPhoto() {
            TMG.IService.GetPhoto(document.getElementById('photoid').value, 1, document.getElementById("typeid").value, UpdatePhoto);
        }
        function PreviousPhoto() {
            TMG.IService.GetPhoto(document.getElementById('photoid').value, -1, document.getElementById("typeid").value, UpdatePhoto);
        }
        function UpdatePhoto(photo) {
            var img = document.createElement('img'); // new Image(1, 1);
            img.onload = function () { updateDlgWidth(this.width, this.height, this); };

            img.src = "<%=System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] %>" + photo.Image;
            
            $("#dlginspiration .imgcontainer img").attr("src", "<%=System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] %>" + photo.Image);
            $("#dlginspiration .text").html(photo.Text);
            document.getElementById("photoid").value = photo.ID;
            document.getElementById("titleurl").value = photo.Link;
            document.getElementById("titletext").value = photo.Title;
            changeFaceBookCommentsURL(getrelurl());
            $('#lnkpage').data("link", 'http://thehomemoneyguide.com' + getrelurl());
            getfbcount(geturl());
            gettwcount(geturl());
            document.getElementById('tmpinit').href = '//pinterest.com/pin/create/button/?url=' + geturle() + '&media=' + encodeURIComponent("<%=System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] %>" + photo.Image) + '&description=Next%20stop%3A%20Pinterest\\';
        }

        /* Social Media */
        function gotoFb() {
            window.open('https://www.facebook.com/sharer.php?s=100&p[title]=' + encodeURIComponent(document.getElementById("titletext").value) + '&p[url]=' + geturle() + '&p[summary]=' + encodeURIComponent(document.getElementById("titletext").value) + '&p[images][0]=' + encodeURIComponent($("#dlgPhotoLeft img").attr("src")), 'Facebook', config = 'height=500,width=680, toolbar=yes, menubar=yes, scrollbars=yes, resizable=yes,location=yes, directories=no, status=yes');
        }
        function gotoTw() {
            window.open('https://twitter.com/intent/tweet?source=sharethiscom&text=' + encodeURIComponent(document.getElementById("titletext").value) + '&' + geturle() + '', 'Twitter', config = 'height=500,width=680, toolbar=yes, menubar=yes, scrollbars=yes, resizable=yes,location=yes, directories=no, status=yes');
        }
        function geturle() {
            return encodeURIComponent(geturl());
        }
        function geturl() {
            return '<%=System.Configuration.ConfigurationManager.AppSettings["baseurl"] %>' + getrelurl();
        }
        function getrelurl() {
            return '/Photos/' + document.getElementById("typeid").value + '/' + document.getElementById("titleurl").value + '/' + getid() + '/';
        }
        function gettext() {
            return encodeURIComponent(document.getElementById('spnName').innerHTML);
        }
        function getid() {
            return document.getElementById("photoid").value;
        }

        function updateDlgWidth(imgw, imgh, obj) {
            $("#dlgPhoto").dialog("option", "position", $("#dlgPhoto").dialog("option", "position", "center"));
        }

        function copyLinkToClipboard() {
            var $temp = $("<input>");
            $("body").append($temp);
            $temp.val($('#lnkpage').data('link')).select();
            document.execCommand("copy");
            $temp.remove();
        }

        /* Dialog Initialization */
        $(document).ready(function () {
            var dlgw = $(window).width() - $(window).width() / 100 * 10;

            if ($(window).width() >= 775)
            {
                dlgw = 735;
            }

            $("#dlgPhoto").dialog({
                autoOpen: false,
                width: dlgw,
                modal: true,
                closeOnEscape: false,
                resizable: false,
                open: function (event, ui) { $('.ui-widget-overlay').bind('click', function () { $("#dlgPhoto").dialog('close'); }); }
            });
            $(".ui-dialog-titlebar").hide();
            $(".ui-dialog-buttonpane").hide();

            var id = document.getElementById('photoid').value;
            if (id.length > 0)
                OpenPhoto(id);
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <bc:Breadcrumbs ID="breadcrumbs1" runat="server" />
    
    <div id="content">
        <asp:Literal runat="server" ID="litID" />
        <h1><asp:Literal runat="server" ID="litTitle" /></h1>
        <div style="text-align:left; font-size:12px; color:black; font-weight:bold; line-height:14px;">
            <br />
            <asp:Literal runat="server" ID="litSubTitle" />
            <hr class="blueline" />
        </div>
        <div id="photos">
            <div class="list" id="photoslist">
                <asp:Literal runat="server" ID="litPhotos" />
            </div>
      
            <div class="clear"></div>
            <br />
              <div class="clear"></div>
            <div id="tmprev" class="prevnext"><a href="javascript:void(0);" onclick="Previous();">< previous</a></div>
            <div id="tmnext" class="prevnext"><a href="javascript:void(0);" onclick="Next();">next ></a></div>
            <div style="clear: both;"></div>
        </div>
        <br />
        <br />
        <div id="dlgPhoto">
            <div style="position: absolute; right: 8px; top: 3px;">
                <a href="javascript:void(0);" onclick="$('#dlgPhoto').dialog('close');">X</a>
            </div>
            <div id="photoArrowLeft">
                <a href="javascript:void(0);" onclick="PreviousPhoto();"><img src="/images/arrowleft.png" /></a>
            </div>
            <div id="dlginspiration">
                <div class="imgcontainer">
                    <img id="imgmain" />
                </div>
                <div class="title">
                    <asp:Literal runat="server" ID="litTitleIn" /></div>
                <div class="text"></div>
                <div class="media">
                    <div class="social facebookshare" onclick="gotoFb();">
                        <div>0</div>
                    </div>
                    <div class="social twitter" onclick="gotoTw();">
                        <div>0</div>
                    </div>
                    <div class="social pinterest">
                        <a target="_blank" id="tmpinit" href="" data-pin-do="buttonPin" data-pin-config="beside"><img src="//assets.pinterest.com/images/pidgets/pin_it_button.png"></a>
                    </div>
                    <div class="social">
                        <a id="lnkpage" onclick="copyLinkToClipboard()"><img src="/images/copylink.png" /></a>
                    </div>
                    <div style="clear: both;"></div>
                </div>
                <div id="fbCommentsPlaceholder">
                    <fbc:FbComments ID="fbComments1" runat="server" Width="100%" />
                </div>
            </div>
            <div id="photoArrowRight">
                <a href="javascript:void(0);" onclick="NextPhoto();"><img src="/images/arrowright.png" /></a>
            </div>
        </div>
    </div>
</asp:Content>

