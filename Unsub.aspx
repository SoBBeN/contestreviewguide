﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage.master" CodeFile="Unsub.aspx.cs" Inherits="Unsub" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <div style="text-align:center; font-size:16px;">
    <asp:Panel runat="server" ID="pnlForm">
        <br />
        <br />
        <br />
        <h1>Unsubscribe from this list by entering your email<br />below and clicking "Unsubscribe"</h1><br />
        <br />
            <asp:Label runat="server" ID="lblEmail"></asp:Label><br />
            <asp:TextBox runat="server" ID="txtEmail" placeholder="Email Address" style="padding:10px; width:400px;" /><br />
        <br />
        <br />
        <asp:Button runat="server" ID="btnSubmit" Text="Unsubscribe" style="padding:10px; font-weight:bold;" onclick="btnSubmit_Click" /><br />
    </asp:Panel>
    <asp:Panel runat="server" ID="pnlSuccess" Visible="false">
        <br />
        <br />
        <br />
        <h1> The address <span runat="server" id="spnEmail"></span> has been successfully unsubscribed.</h1><br />
        <br />
        <br />
        <br />
        <br />
        <br />
        <br />
    </asp:Panel>

    </div>
</asp:Content>