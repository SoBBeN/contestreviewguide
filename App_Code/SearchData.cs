﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for SearchData
/// </summary>
/// 
[Serializable]
public class SearchData
{
    private int babyNameCount;

    public int BabyNameCount
    {
        get { return babyNameCount; }
        set { babyNameCount = value; }
    }
}