﻿using DB;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Runtime.Serialization;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for UserSession
/// </summary>
[Serializable()]    //Set this attribute to all the classes that want to serialize
public class UserSession : ISerializable
{
    private int sessionId;
    private Guid guid;
    private int campaignId;
    private string subId;
    private string affId;
    private DataTable babyNameSearchList;
    private int babyNameSearchListLastMax;
    private int userId = int.MinValue;
    private string username;
    private string email;

    public UserSession()
    {
        InitializeSession();
    }

    private UserSession(string guid)
    {
        SqlDataReader dr = DbFunctions.GetUser(guid);
        if (dr.Read())
        {
            string strReferrer = GetDefaultValue(HttpContext.Current.Request.QueryString, "r", "");
            subId = GetDefaultValue(HttpContext.Current.Request.QueryString, "referid", "");
            email = Convert.ToString(dr["Email"]);
            username = Convert.ToString(dr["Firstname"]) + ' ' + Convert.ToString(dr["Lastname"]).Substring(0, 1) + '.';
            userId = Convert.ToInt32(dr["UserID"]);
            sessionId = Convert.ToInt32(dr["SessionID"]);
            string strUrlReferrer = Convert.ToString(dr["HTTPReferrer"]);
            affId = Convert.ToString(dr["AffID"]);
            string hitId = GetDefaultValue(HttpContext.Current.Request.QueryString, "hitId", "");
            this.guid = new Guid(guid);

            if (!int.TryParse(HttpContext.Current.Request.QueryString.Get("c"), out campaignId))
                campaignId = 0;

        }
    }

    public UserSession(SerializationInfo info, StreamingContext ctxt)
    {
        userId = (int)info.GetValue("userId", typeof(int));
        username = (String)info.GetValue("username", typeof(string));
        email = (String)info.GetValue("email", typeof(string));
        sessionId = (int)info.GetValue("sessionId", typeof(int));
        guid = (Guid)info.GetValue("guid", typeof(Guid));
        campaignId = (int)info.GetValue("campaignId", typeof(int));
        subId = (String)info.GetValue("subId", typeof(string));
        affId = (String)info.GetValue("affId", typeof(string));
    }

    public void GetObjectData(SerializationInfo info, StreamingContext context)
    {
        info.AddValue("userId", userId);
        info.AddValue("username", username);
        info.AddValue("email", email);
        info.AddValue("sessionId", sessionId);
        info.AddValue("guid", guid);
        info.AddValue("campaignId", campaignId);
        info.AddValue("subId", subId);
        info.AddValue("affId", affId);
    }

    public int UserID
    {
        get { return userId; }
    }
    public string Username
    {
        get { return username; }
    }
    public string Email
    {
        get { return email; }
    }
    public void SetUser(int userId, string username, string email)
    {
        this.userId = userId;
        this.username = username;
        this.email = email;

        HttpCookie hc = new HttpCookie("SessionID", sessionId.ToString());
        hc.Domain = ".foundmoneyguide.com"; // must start with "."
        hc.Expires = DateTime.Now.AddMonths(3);
        HttpContext.Current.Response.Cookies.Add(hc);

        if (sessionId > 0)
        {
            SqlServer objSqlServer = new SqlServer();
            StringBuilder query = new StringBuilder("[BF_Session_Update] ");

            query.Append(objSqlServer.FormatSql(sessionId)).Append(",");
            query.Append(objSqlServer.FormatSql(userId));

            try
            {
                objSqlServer.ExecNonQuery(query.ToString());
            }
            catch (Exception ex)
            {
                sessionId = -1;
                ErrorHandling.SendException("UserSession.InitializeSession BF_Session_Update", ex);
            }
        }
    }
    public void Logout()
    {
        this.userId = -1;
        this.username = null;
        this.email = null;
    }

    public int SessionID
    {
        get { return sessionId; }
    }
    public Guid Guid
    {
        get { return guid; }
    }
    public int CampaignID
    {
        get { return campaignId; }
    }
    public string SubID
    {
        get { return subId; }
    }
    public string AffiliateID
    {
        get { return affId; }
    }

    public DataTable BabyNameSearchList
    {
        get { return babyNameSearchList; }
        set { babyNameSearchList = value; }
    }
    public int BabyNameSearchListLastMax
    {
        get { return babyNameSearchListLastMax; }
        set { babyNameSearchListLastMax = value; }
    }

    public static void InsertImpression()
    {
        GetInstance(true);
    }

    public static UserSession GetInstance()
    {
        return GetInstance(false);
    }

    public static UserSession GetInstance(bool insertImpression)
    {
        UserSession instance = null;

        if ((HttpContext.Current.Session["UserSession"] != null))
        {
            instance = (UserSession)HttpContext.Current.Session["UserSession"];
        }
        else
        {
            instance = new UserSession();
            HttpContext.Current.Session["UserSession"] = instance;
        }

        if (insertImpression)
            instance.PrivateInsertImpression();

        return instance;
    }

    private void InitializeSession()
    {
        string strReferrer = GetDefaultValue(HttpContext.Current.Request.QueryString, "r", "");
        subId = GetDefaultValue(HttpContext.Current.Request.QueryString, "referid", "");
        email = GetDefaultValue(HttpContext.Current.Request.QueryString, "email", "");
        string strUrlReferrer = HttpContext.Current.Request.UrlReferrer == null ? "" : HttpContext.Current.Request.UrlReferrer.ToString();
        affId = GetDefaultValue(HttpContext.Current.Request.QueryString, "affId", "");
        string hitId = GetDefaultValue(HttpContext.Current.Request.QueryString, "hitId", "");
        guid = Guid.NewGuid();

        if (!int.TryParse(HttpContext.Current.Request.QueryString.Get("c"), out campaignId))
            campaignId = 0;

        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[BF_Session_Insert] ");

        query.Append(objSqlServer.FormatSql(guid.ToString())).Append(",");
        query.Append(objSqlServer.FormatSql(userId)).Append(",");
        query.Append(objSqlServer.FormatSql(HttpContext.Current.Request.UserAgent)).Append(",");
        query.Append(objSqlServer.FormatSql(strUrlReferrer)).Append(",");
        query.Append(objSqlServer.FormatSql(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"])).Append(",");
        query.Append(objSqlServer.FormatSql(HttpContext.Current.Request.RawUrl)).Append(",");
        query.Append(objSqlServer.FormatSql(Convert.ToInt32(System.Configuration.ConfigurationManager.AppSettings["websiteid"])));

        try
        {
            sessionId = Convert.ToInt32(objSqlServer.GetScalar(query.ToString()));
        }
        catch (Exception ex)
        {
            sessionId = -1;
            ErrorHandling.SendException("UserSession.InitializeSession BF_Session_Insert", ex);
        }

    }

    private static string GetDefaultValue(System.Collections.Specialized.NameValueCollection Collection, string Name, string Default)
    {
        return Collection[Name] == null ? Default : Collection[Name];
    }

    private void PrivateInsertImpression()
    {
        SqlServer objSqlServer = new SqlServer();
        StringBuilder query = new StringBuilder("[BF_InsertImpression] ");

        query.Append(objSqlServer.FormatSql(HttpContext.Current.Request.UrlReferrer == null ? null : HttpContext.Current.Request.UrlReferrer.ToString())).Append(",");
        query.Append(objSqlServer.FormatSql(GetPage())).Append(",");
        query.Append(objSqlServer.FormatSql(SessionID));

        try
        {
            objSqlServer.ExecNonQuery(query.ToString());
        }
        catch (Exception ex)
        {
            ErrorHandling.SendException("UserSession.PrivateInsertImpression BF_InsertImpression", ex);
        }

    }

    private static string GetPage()
    {
        string url = HttpContext.Current.Request.Url.AbsolutePath.ToLower();

        //if (url.EndsWith("/"))
        //    url += "default.aspx";

        return url;
    }

    public static UserSession RestoreSessionFromGUID(string guid)
    {
        UserSession instance = new UserSession(guid);
        return instance;
    }
}