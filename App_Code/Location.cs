﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Text.RegularExpressions;
using System.Configuration;

namespace NSS.ROS
{
    /// <summary>
    /// ISO-3166 location code parser
    /// </summary>
    public class Location
    {
        private string _strCountryCode;
        private string _strRegionCode;
        private string _strCode;

        public Location(string Code)
        {
            _strCode = Code;
            Parse();
        }

        private void Parse()
        {
            Match m = Regex.Match(_strCode, "^([\\w]{2})($|-([\\w\\d]{2,3})$)");
            _strCountryCode = m.Groups[1].Value;
            _strRegionCode = m.Groups[3].Value;
        }

        public string CountryCode
        {
            get
            {
                return _strCountryCode;
            }
        }

        public string RegionCode
        {
            get
            {
                return _strRegionCode;
            }
        }

        public string Code
        {
            get
            {
                return _strCode;
            }
        }
    }
}