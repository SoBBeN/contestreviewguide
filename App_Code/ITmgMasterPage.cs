﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

/// <summary>
/// Summary description for ITmgMasterPage
/// </summary>
public class ITmgMasterPage : System.Web.UI.MasterPage
{
    //protected string pagelogo = System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "logo.png";
    protected string pagelogo = System.Configuration.ConfigurationManager.AppSettings["AssetPath"] + "/images/" + "contestreview_logo.png";
    protected string pagetitle = System.Configuration.ConfigurationManager.AppSettings["title"];
    protected string pageurl = System.Configuration.ConfigurationManager.AppSettings["baseurl"];
    protected string pagetype = "website";
    protected string pagedescription = String.Empty;
    protected List<string> pageimg = new List<string>();
    protected bool showOGtags = true;

    public string PageLogo
    {
        get { return pagelogo; }
        set {
            if (value.StartsWith("http"))
                pagelogo = value;
            else
                pagelogo = System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + value;
        }
    }
    public string PageTitle
    {
        get { return pagetitle; }
        set { pagetitle = value; }
    }
    public string PageURL
    {
        get { return pageurl; }
        set { pageurl = System.Configuration.ConfigurationManager.AppSettings["baseurl"] + value; }
    }
    public string PageType
    {
        get { return pagetype; }
        set { pagetype = value; }
    }
    public bool ShowOGtags
    {
        get { return showOGtags; }
        set { showOGtags = value; }
    }
    public string PageDescription
    {
        get { return pagedescription; }
        set
        {
            pagedescription = Functions.RemoveHtml(value);
            if (pagedescription.Length > 220)
            {
                pagedescription = pagedescription.Substring(0, 220);
                pagedescription = pagedescription.Substring(0, pagedescription.LastIndexOf(" ")) + " ...";
            }
            pagedescription = pagedescription.Trim();
        }
    }
    public void AddImage(string img)
    {
        pageimg.Add(img);
    }
}