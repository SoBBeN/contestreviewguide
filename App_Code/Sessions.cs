﻿using System;
using System.Data;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Security;
using System.Web.UI;
using System.Collections;

/// <summary>
/// Utility to access visitor and UI sessions
/// </summary>
public static class Sessions
{

    /// <summary>
    /// UI session
    /// </summary>
    public static Hashtable UI
    {
        get { 
            return HttpContext.Current.Session["UIData"] == null
                ? null : (Hashtable)HttpContext.Current.Session["UIData"]; 
        }
    }

    /// <summary>
    /// Visitor session
    /// </summary>
    public static Hashtable Visitor
    {
        get { 
            return HttpContext.Current.Session["VisitorData"] == null
                ? null : (Hashtable)HttpContext.Current.Session["VisitorData"];
        }
    }

    /// <summary>
    /// Creates or recreates (clears) the UI session's data
    /// </summary>
    public static void InitUISession()
    {
        HttpContext.Current.Session["UIData"] = new Hashtable();
    }

    /// <summary>
    /// Creates or recreates (clears) the visitor session's data
    /// </summary>
    public static void InitVisitorSession()
    {
        HttpContext.Current.Session["VisitorData"] = new Hashtable();
    }
}
