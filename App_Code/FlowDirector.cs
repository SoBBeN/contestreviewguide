﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.SessionState;
using System.Data;
using System.Configuration;
using System.Collections;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Diagnostics;
using System.Linq;
using DB;

namespace NSS.ROS
{
    /// <summary>
    /// Flow director (site hosting).
    /// </summary>
    public static class FlowDirector
    {
        public static void AssertSessionIntegrity(bool clearSession = false)
        {
            AssertSessionIntegrity(-1, clearSession);
        }

        public static void AssertSessionIntegrity(int webSiteIdIfNotDetected, bool clearSession = false)
        {
            AssertSessionIntegrity(webSiteIdIfNotDetected, true, clearSession);
        }

        public static void AssertSessionIntegrity(int webSiteIdIfNotDetected, bool allowRedirect, bool clearSession, bool postBack)
        {
            if (!postBack)
            {
                FlowDirector.AssertSessionIntegrity(webSiteIdIfNotDetected, allowRedirect, clearSession);
            }
            else
            {
                FlowDirector.AssertSessionIntegrity(webSiteIdIfNotDetected, false, false);
            }
        }

        public static void AssertSessionIntegrity(int webSiteIdIfNotDetected, bool allowRedirect, bool clearSession)
        {
            HttpRequest Request = HttpContext.Current.Request;
            bool isPostBack = false;
            Page pageHandler = HttpContext.Current.CurrentHandler as Page;
            if (pageHandler != null)
                isPostBack = pageHandler.IsPostBack;

            if (HttpContext.Current.Session["clear"] == null)
                HttpContext.Current.Session["clear"] = 0;

            if (isPostBack == false && clearSession)
            {
                HttpContext.Current.Session.Clear();
                HttpContext.Current.Session["clear"] = 1;
            }

            if (isPostBack == false && !String.IsNullOrEmpty(Request.QueryString["transaction_id"]) && (HttpContext.Current.Session["TransactionID"] == null || Convert.ToString(HttpContext.Current.Session["TransactionID"]) != Request.QueryString["transaction_id"]))
                HttpContext.Current.Session["TransactionID"] = Request.QueryString["transaction_id"];

            if (Sessions.Visitor == null || Sessions.Visitor["SessionID"] == null)
            {
                if (!String.IsNullOrEmpty(Request.QueryString["referid"]))
                {
                    HttpContext.Current.Session["RefererID"] = Request.QueryString["referid"];
                }
                else
                {
                    HttpContext.Current.Session["RefererID"] = "HOME";
                }

                LaunchSession(webSiteIdIfNotDetected, allowRedirect, clearSession);
            }
            else if (!String.IsNullOrEmpty(Request.QueryString["referid"]))
            {
                if (Request.QueryString["redir"] == "0" && isPostBack == false && !String.IsNullOrEmpty(Request.QueryString["referid"]) && (HttpContext.Current.Session["RefererID"] == null || Convert.ToString(HttpContext.Current.Session["RefererID"]) != Request.QueryString["referid"]))
                {
                    //ErrorHandling.SendException("Reset ReferID", new Exception(), "Old ReferID:" + Convert.ToString(HttpContext.Current.Session["RefererID"]) + "\nNew ReferID:" + Request.QueryString["referid"]);
                    HttpContext.Current.Session["RefererID"] = Request.QueryString["referid"];
                    LaunchSession(webSiteIdIfNotDetected, allowRedirect, clearSession);
                }
            }
        }

        public static void BustFrame(string URL)
        {
            BustFrame(URL, false);
        }
        public static void BustFrame(string URL, bool sendNotice)
        {
            HttpRequest Request = HttpContext.Current.Request;

            bool isPostBack = false;
            Page pageHandler = HttpContext.Current.CurrentHandler as Page;
            if (pageHandler != null)
                isPostBack = pageHandler.IsPostBack;

            if (!String.IsNullOrEmpty(Request["redir"]) && Request["redir"] == "0" && isPostBack == false)
                return;
            
            Hashtable session = Sessions.Visitor;
            DataRow rowCheckStatic = ros.Flow_GetOfferDetails((int)session["CampaignID"], (int)session["PreviousOfferID"]);

            StringBuilder sb = new StringBuilder();

            foreach (String key in Request.QueryString.AllKeys)
            {
                if (!URL.Replace('?', '&').Contains("&" + key + "="))
                {
                    if (sb.Length == 0 && !URL.Contains("?"))
                        sb.Append("?");
                    else
                        sb.Append("&");
                    sb.Append(key).Append("=").Append(HttpUtility.UrlEncode(Request.QueryString[key]));
                }
            }

            URL += sb.ToString();

            //if (sendNotice)
            //    ErrorHandling.SendException("Bust Frame URLs", new Exception(), "Current URL:" + HttpContext.Current.Request.Url.AbsoluteUri + "\nNew URL:" + URL);

            HttpContext.Current.Response.Redirect(URL);
        }

        public static int SaveLead()
        {
            Hashtable session = Sessions.Visitor;
            Hashtable lead = (Hashtable)session["Lead"];
            return ros.Flow_SaveLead((int)session["OfferImpressionID"], lead);
        }

        public static void CompleteLandingPage(bool leadAlreadySaved = false, bool checkGAPCDirectSave = false)
        {
            Hashtable session = Sessions.Visitor;
            Hashtable lead = (Hashtable)session["Lead"];
            bool existsSurveyPage = false;
            int SurveyPageID;

            session["LandingPageCompleted"] = true;
            if (!leadAlreadySaved)
                session["leadid"] = ros.Flow_SaveLead((int)session["OfferImpressionID"], lead);

            NormalizeData(lead);
            if (((short)lead["_NormalAge"]) > 0 || ((string)lead["_NormalGender"]).Length > 0 || ((int)lead["_NormalIncome"]) > 0)
            {
                ros.Flow_UpdateLeadNormals((int)session["OfferImpressionID"],
                    (short)lead["_NormalAge"], (string)lead["_NormalGender"], (int)lead["_NormalIncome"]);
            }

            if (checkGAPCDirectSave)
                Functions.PullGAPCDirectSaveIntoLead();

            ros.Flow_FlagOfferImpressionAsCompleted((int)session["OfferImpressionID"]);
            Feed.EngageFeeds((int)session["OfferImpressionID"]);
            SelectFlow();
            if (!(bool)session["FlowIsStatic"])
            {
                DecisionEngine.CompileInitialScores();
            }

            existsSurveyPage = ros.Flow_CheckSurveyPageInFlow((int)session["FlowID"]);
            if (existsSurveyPage == true)
            {
                SurveyPageID = ros.Flow_GetSurveyPageID((int)session["FlowID"]);
                LoadSurveyPage(SurveyPageID);
            }
            else
            {
                PresentNextOffer();
            }
        }

        private static void NormalizeData(Hashtable lead)
        {
            lead["_NormalAge"] = ros.Analytics_GetNormalizedAge(Utility.GetDerivedAge(lead["dateofbirth"]));
            lead["_NormalGender"] = Utility.GetNormalizedGender(lead["gender"]);
            lead["_NormalIncome"] = Utility.GetNormalizedIncome(lead["income"]);
        }

        public static void CompleteOffer()
        {
            CompleteOffer(false);
        }

        public static void CompleteOffer(bool skipsave = false, bool offerAccepted = true)
        {
            // -> replace CompleteLandingPage (and integrate its functionality) with this function?  
            //      -> ^^^ this would require checking LandingPageCompleted here

            Hashtable session = Sessions.Visitor;

            if (offerAccepted)
            {
                if (session["AcceptedOfferIDs"] == null)
                {
                    session["AcceptedOfferIDs"] = ",";
                }

                session["AcceptedOfferIDs"] = session["AcceptedOfferIDs"].ToString() + session["CurrentOfferID"].ToString() + ",";
            }

            if (session["OfferImpressionID"] != null)
            {
                Hashtable lead = (Hashtable) session["OfferLead"];
                if (lead == null || lead.Count == 0)
                    lead = FlowDirector.CurrentLead;
                if (!skipsave)
                    session["leadid"] = ros.Flow_SaveLead((int)session["OfferImpressionID"], lead);

                ros.Flow_FlagOfferImpressionAsCompleted((int)session["OfferImpressionID"]);
                Feed.EngageFeeds((int)session["OfferImpressionID"]);
            }
            PresentNextOffer();
        }

        public static void CompleteOffer_withURLRedirect(string URL)
        {
            CompleteOffer_withURLRedirect(URL, false);
        }

        public static void CompleteOffer_withURLRedirect(string URL, bool skipsave)
        {
            // -> replace CompleteLandingPage (and integrate its functionality) with this function?  
            //      -> ^^^ this would require checking LandingPageCompleted here

            Hashtable session = Sessions.Visitor;
            Hashtable lead = (Hashtable)session["OfferLead"];
            if (lead == null || lead.Count == 0)
                lead = FlowDirector.CurrentLead;
            if (!skipsave)
                session["leadid"] = ros.Flow_SaveLead((int)session["OfferImpressionID"], lead);

            ros.Flow_FlagOfferImpressionAsCompleted((int)session["OfferImpressionID"]);
            Feed.EngageFeeds((int)session["OfferImpressionID"]);
            //PresentNextOffer();

            HttpContext.Current.Response.Redirect(URL);
        }

        public static void CompleteOffer_withoutURLRedirect(bool skipsave)
        {
            // -> replace CompleteLandingPage (and integrate its functionality) with this function?  
            //      -> ^^^ this would require checking LandingPageCompleted here

            Hashtable session = Sessions.Visitor;
            Hashtable lead = (Hashtable)session["Lead"];
            if (!skipsave)
                session["leadid"] = ros.Flow_SaveLead((int)session["OfferImpressionID"], lead);

            ros.Flow_FlagOfferImpressionAsCompleted((int)session["OfferImpressionID"]);
            Feed.EngageFeeds((int)session["OfferImpressionID"]);
        }

        public static void LoadSurveyPage(int SurveyPageID)
        {
            Hashtable session = Sessions.Visitor;

            // clear offer lead data
            session["OfferLead"] = new Hashtable();

            // update PreviousOfferID BEFORE calling GetNextOffer; required by DecisionEngine
            session["PreviousOfferID"] = session["CurrentOfferID"];

            //ImpressionProvider provider = ImpressionProvider.Unspecified;
            BustFrame("/hosting/offer.aspx?OfferID=0&SurveyPageID=" + SurveyPageID.ToString());
        }

        public static void PresentNextOffer()
        {
            if (Sessions.Visitor == null)
                Sessions.InitVisitorSession();
            
            Hashtable session = Sessions.Visitor;

            // clear offer lead data
            session["OfferLead"] = new Hashtable();

            // update PreviousOfferID BEFORE calling GetNextOffer; required by DecisionEngine
            session["PreviousOfferID"] = session["CurrentOfferID"];

            ImpressionProvider provider = ImpressionProvider.Unspecified;
            DataRow row = null;

            // this can be commented out, since we'll be doing FollowUpOfferIds
            /*if (session["ForceNextOfferId"] != null)
            {
                DataTable dt = new DataTable();
                dt.Columns.Add("OfferID");
                row = dt.NewRow();
                row["OfferID"] = session["ForceNextOfferId"].ToString();
                session["ForceNextOfferId"] = null;
            }*/

            if (row == null && ((bool)session["FlowIsStatic"] || (int)session["OfferIndex"] < (int)session["DynamicFlowMaxOffers"]))
                row = GetNextOffer(out provider);

            if (row == null)
            {
                provider = ImpressionProvider.ConfirmationPage;
                int intFinalPageOfferID = (int)session["FinalPageOfferID"];

                session["OfferIndex"] = (int)session["OfferIndex"] + 1;
                session["CurrentOfferID"] = intFinalPageOfferID;

                session["OfferImpressionID"] = ros.Flow_InsertOfferImpression(
                    (int)session["SessionID"], intFinalPageOfferID, (int)session["PreviousOfferID"], provider);

                ros.Flow_UpdateSessionFinalPage((int)session["SessionID"], (int)session["OfferImpressionID"]);

                DataRow rowCheckStatic = ros.Flow_GetOfferDetails((int)session["CampaignID"], (int)session["PreviousOfferID"]);

                BustFrame("/hosting/complete.aspx");
            }
            else
            {
                int intOfferID = Convert.ToInt32(row["OfferID"]);

                session["OfferIndex"] = (int)session["OfferIndex"] + 1; // counter; used by GetNextOffer()
                session["FollowUpOfferID"] = row["FollowUpOfferID"] != DBNull.Value ? Convert.ToInt32(row["FollowUpOfferID"].ToString()) : 0;
                session["CurrentOfferID"] = intOfferID;
                session["OfferImpressionID"] = ros.Flow_InsertOfferImpression(
                    (int)session["SessionID"], intOfferID, (int)session["PreviousOfferID"], provider);

                BustFrame("/hosting/offer.aspx?offerid=" + intOfferID.ToString());
            }
        }

        private static DataRow GetNextOffer(out ImpressionProvider Provider)
        {
            DataRow ret = null;

            Hashtable session = Sessions.Visitor;
            bool FlowOfferPositionShouldBeSeeded = false;

            DataTable dtOffers = ros.Flow_GetFilteredOffers(CurrentSessionID);

            FilterOffersByConditions(dtOffers);
            if (dtOffers.Rows.Count == 0)
            {
                // no offers left
                Provider = ImpressionProvider.Unspecified;
                return null;
            }

            if ((bool)session["FlowIsStatic"])
            {
                // static: get next offer
                ret = dtOffers.Rows[0];
                Provider = ImpressionProvider.StaticFlow;
            }
            else
            {
                //// dynamic: use decision engine (or randomizer for the first offer)
                //bool bUseRandomizer;

                //// first offer: use random offer occasionally (RandomizerUsagePct)
                //Random r = new Random(DateTime.Now.Millisecond);
                //if ((int)session["OfferIndex"] == 0)
                //    bUseRandomizer = int.Parse(GetSetting("RandomizerUsagePct")) > r.Next(0, 100);
                //else
                //    bUseRandomizer = false;

                //if (bUseRandomizer)
                //{
                //    // select random offer
                //    ret = dtOffers.Rows[r.Next(dtOffers.Rows.Count)];
                //    Provider = ImpressionProvider.Randomizer;
                //}
                //else
                //{
                //    // decision engine
                //    ret = DecisionEngine.DrawBestOffer(dtOffers);
                //    Provider = ImpressionProvider.DecisionEngine;
                //

                // dynamic: use decision engine or randomizer
                bool bUseRandomizer;

                // use random offer occasionally (RandomizerUsagePct)
                Random r = new Random(DateTime.Now.Millisecond);
                bUseRandomizer = int.Parse(Utility.GetSetting("RandomizerUsagePct")) > r.Next(0, 100);
                if (bUseRandomizer)
                {
                    // use random offer; if no suitable random offer can be found, the decision engine will be used (ret == null)
                    ret = DecisionEngine.GetRandomOffer(dtOffers);
                }

                if (ret != null)
                {
                    Provider = ImpressionProvider.Randomizer;
                }
                else
                {
                    int TempOfferIndex = Convert.ToInt32(session["OfferIndex"]);
                    TempOfferIndex++;
                    int TempCampaignID = Convert.ToInt32(session["CampaignID"]);
                    FlowOfferPositionShouldBeSeeded = DecisionEngine.CheckSeedingSystem(TempCampaignID, TempOfferIndex);

                    if (FlowOfferPositionShouldBeSeeded)
                    {
                        // use decision engine
                        ret = DecisionEngine.DrawSeededOffer(TempCampaignID, TempOfferIndex);
                        Provider = ImpressionProvider.DecisionEngine;

                    }
                    else
                    {
                        // use decision engine
                        ret = DecisionEngine.DrawBestOffer(dtOffers);
                        Provider = ImpressionProvider.DecisionEngine;
                    }
                }
            }

            return ret;
        }

        private static void FilterOffersByConditions(DataTable Offers)
        {
            // [UNTESTED]
            // [INCOMPLETE]

            Hashtable Session = Sessions.Visitor;

            // remove offers whose conditions don't fit the visitor's profile
            foreach (DataRow rowOffer in Offers.Rows)
            {
                bool bAllow = true;

                int intOfferID = Convert.ToInt32(rowOffer["OfferID"]);
                DataTable dt = ros.Flow_GetOfferConditions(intOfferID);
                foreach (DataRow rowCond in dt.Rows)
                {
                    bAllow &= AssertCondition(
                        rowCond["Field"].ToString(),
                        rowCond["Operator"].ToString(),
                        rowCond["Value"].ToString());

                    if (!bAllow)
                        break;  // quit checking conditions as soon as one fails
                }

                if (!bAllow)
                {
                    rowOffer["OfferID"] = DBNull.Value;  // flag as invalid
                    if(rowOffer["CapFailoverOfferId"] != DBNull.Value && rowOffer["CFOIsOnSchedule"].ToString() == "1")
                    {
                        int intFailoverOfferId = Convert.ToInt32(rowOffer["CapFailoverOfferId"]);
                        DataTable dtF = ros.Flow_GetOfferConditions(intFailoverOfferId);
                        bool bAllowF = true;
                        foreach (DataRow rowCond in dt.Rows)
                        {
                            bAllowF &= AssertCondition(
                                rowCond["Field"].ToString(),
                                rowCond["Operator"].ToString(),
                                rowCond["Value"].ToString());

                            if (!bAllowF)
                                break;  // quit checking conditions as soon as one fails
                        }

                        if (bAllowF)
                            rowOffer["OfferID"] = intFailoverOfferId;
                        
                    }
                }

            }

            // remove flagged offers
            for (int i = Offers.Rows.Count - 1; i >= 0; i--)
            {
                if (Offers.Rows[i]["OfferID"] == DBNull.Value)
                    Offers.Rows.RemoveAt(i);
            }
        }

        private static bool AssertCondition(string Field, string Operator, string Value)
        {
            // [TESTED: OK]

            Hashtable lead = CurrentLead;

            switch (Field)
            {
                case "age":         // operators: eq, neq, lt, lte, gte, gt
                    int intConditionAge = int.Parse(Value);
                    int intUserAge = Utility.GetDerivedAge(GetLeadValue(lead, "DateOfBirth"));

                    // if age is unknown (or invalid), the condition fails
                    if (intUserAge == int.MinValue)
                        return false;

                    switch (Operator)
                    {
                        case "eq":
                            return intUserAge == intConditionAge;

                        case "neq":
                            return intUserAge != intConditionAge;

                        case "lt":
                            return intUserAge < intConditionAge;

                        case "lte":
                            return intUserAge <= intConditionAge;

                        case "gte":
                            return intUserAge >= intConditionAge;

                        case "gt":
                            return intUserAge > intConditionAge;
                        default:
                            return true;
                    }
                case "state":
                    switch(Operator)
                    {
                        case "eq": // Equals
                        case "in": // In
                            return Value.Contains(GetLeadValue(lead, "state"));
                        case "neq": // Not Equals
                        case "nin": // Not In
                            return !Value.Contains(GetLeadValue(lead, "state"));
                        default:
                            return true;
                    }
                case "gender":      // operators: eq, neq
                    switch (Operator)
                    {
                        case "eq":
                            return GetLeadValue(lead, "Gender") == Value;

                        case "neq":
                            return GetLeadValue(lead, "Gender") != Value;
                        default:
                            return true;
                    }
                case "location":    // operators: eq, neq
                    // [TESTED: OK]
                    string[] strLocations = Value.Split(',');
                    Location locUser = new Location(Sessions.Visitor["LocationCode"].ToString());
                    bool bUserInRegion = true;

                    // determine whether or not the user is in one the locations specified in the condition
                    for (int i = 0; i < strLocations.Length; i++)
                    {
                        Location locCondition = new Location(strLocations[i]);
                        if (locCondition.RegionCode.Length == 0)
                        {
                            // country
                            bUserInRegion = locUser.CountryCode == locCondition.CountryCode;
                        }
                        else
                        {
                            // region
                            bUserInRegion = locUser.Code == locCondition.Code;
                        }

                        if (bUserInRegion)
                            break;
                    }

                    switch (Operator)
                    {
                        case "eq":
                            return bUserInRegion;

                        case "neq":
                            return !bUserInRegion;

                        default:
                            return true;
                    }
                case "source":
                    string sourceId = Functions.GetAffiliateName(true);
                    switch (Operator)
                    {
                        case "eq": // Equals
                        case "in": // In
                            return Value.Contains(sourceId);
                        case "neq": // Not Equals
                        case "nin": // Not In
                            return !Value.Contains(sourceId);
                        default:
                            return true;
                    }
                case "accepted":
                    if (Sessions.Visitor["AcceptedOfferIDs"] == null)
                        return false;
                    else
                    {
                        string compareValue = "," + Value + ",";
                        switch (Operator)
                        {
                            case "eq":
                            case "in":
                                return Sessions.Visitor["AcceptedOfferIDs"].ToString().Contains(compareValue);
                            case "neq":
                            case "nin":
                                return !Sessions.Visitor["AcceptedOfferIDs"].ToString().Contains(compareValue);
                            default:
                                return false;
                        }
                    }
                    break;
                case "briteverify":
                    if (Sessions.Visitor["briteverify"] == null)
                        return true;
                    else
                    {
                        var existingBf = (Hashtable)Procurios.Public.JSON.JsonDecode(Sessions.Visitor["briteverify"].ToString());

                        if (existingBf.ContainsKey(Value))
                        {
                            switch (Operator)
                            {
                                case "vld":
                                    return ((Hashtable)((Hashtable)existingBf)[Value])["status"].ToString() == "valid";
                                case "ild":
                                    return ((Hashtable)((Hashtable)existingBf)[Value])["status"].ToString() == "invalid";
                                default:
                                    return false;
                            }
                        }

                        return true;
                    }
                default:
                    return true;
            }
            
        }

        public static void LaunchSession(int webSiteIdIfNotDetected, bool allowRedirect, bool clearSession = false)
        {
            // launches the session:
            //  - by cid if specified
            //  - by host name   

            HttpRequest Request = HttpContext.Current.Request;
            HttpResponse Response = HttpContext.Current.Response;

            //Response.Write("a");
            bool launchedByCampaign;
            if (Request["c"] != null)
            {
                try
                {
                    LaunchByCampaignCode(Request["c"], allowRedirect, clearSession);
                    launchedByCampaign = true;
                }
                catch
                {
                    launchedByCampaign = false;
                }
            }
            else
                launchedByCampaign = false;

            if (!launchedByCampaign)
            {
                int intWebsiteID = -1;

                try
                {
                    intWebsiteID = ros.Flow_GetWebsiteIDByDomainName(Request.Url.Host);
                    if (intWebsiteID <= 0 && webSiteIdIfNotDetected < 0)
                        throw new Exception("No website found");
                }
                catch (Exception ex)
                {
                    ErrorHandling.SendException("Flow_GetWebsiteIDByDomainName", ex, "Host:" + Request.Url.Host + "\nintWebsiteID:" + intWebsiteID.ToString());
                }

                if (intWebsiteID > 0)
                    LaunchByWebsiteID(intWebsiteID, allowRedirect, clearSession);
                else if (webSiteIdIfNotDetected > 0)
                    LaunchByWebsiteID(webSiteIdIfNotDetected, allowRedirect, clearSession);
                else
                {
                    ErrorHandling.SendException("LaunchSession", new Exception("Invalid WebSite"), String.Format("Current URL: {0}\nWebsiteID: {1}", HttpContext.Current.Request.Url.AbsoluteUri, intWebsiteID));
                    Response.Write(intWebsiteID.ToString());
                    Response.Redirect("/hosting/invalid-website.aspx");
                    Response.End();
                }
            }
        }

        private static void LaunchByWebsiteID(int WebsiteID, bool allowRedirect, bool clearSession)
        {
            Retries retry = default(Retries);
            retry = new Retries(3);
            DataRow dr = null;
            while (retry.NeedsRetry())
            {
                try
                {
                    dr = ros.Flow_GetDefaultCampaignDetails(WebsiteID);
                }
                catch (Exception ex)
                {
                    retry.HandleException(ref ex, "Flow_GetDefaultCampaignDetails", "Host:" + HttpContext.Current.Request.Url.Host + "\nWebsiteID:" + WebsiteID.ToString(), true);
                    dr = null;
                }
            }
            if (dr != null)
                InitializeSession(dr, allowRedirect, clearSession);
            else
                ErrorHandling.SendException("LaunchByWebsiteID", new Exception("No CampaignDetails Found"), String.Format("Current URL: {0}\nWebsiteID: {1}", HttpContext.Current.Request.Url.AbsoluteUri, WebsiteID));
        }

        private static void LaunchByCampaignCode(string CampaignCode, bool allowRedirect, bool clearSession)
        {
            Retries retry = default(Retries);
            retry = new Retries(3);
            DataRow dr = null;
            while (retry.NeedsRetry())
            {
                try
                {
                    dr = ros.Flow_GetCampaignDetailsByCode(CampaignCode);
                }
                catch (Exception ex)
                {
                    retry.HandleException(ref ex, "Flow_GetCampaignDetailsByCode", "Host:" + HttpContext.Current.Request.Url.Host + "\nCampaignCode:" + CampaignCode.ToString(), true);
                    dr = null;
                }
            }
            if (dr != null)
                InitializeSession(dr, allowRedirect, clearSession);
            else
            {
                throw new Exception(String.Format("No CampaignDetails Found : Current URL: {0}\nCampaignCode: {1}", HttpContext.Current.Request.Url.AbsoluteUri, CampaignCode));
            }
        }

        public static void InitializeSessionFromRecordID(int recordid, int campaignid, string campaignCode)
        {
            DBCON aDB3 = new DBCON();

            aDB3.SqlCommand.CommandText = "Session_GetLatestFromRecordID";
            aDB3.SqlCommand.Parameters.AddWithValue("@recordid", recordid);
            aDB3.SqlCommand.Parameters.AddWithValue("@campaignid", campaignid);

            object obj = aDB3.ExecuteScalar();

            if (obj != DBNull.Value)
            {
                InitializeSessionReInit(campaignCode, Convert.ToInt32(obj));
            }
        }


        public static void InitializeSessionReInit(string CampaignCode, int SessionID)
        {
            DataRow CampaignData = ros.Flow_GetCampaignDetailsByCode(CampaignCode);
            HttpContext context = HttpContext.Current;
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            // init visitor session only (session data is stored in a hashtable separate from the UI session)
            Sessions.InitVisitorSession();
            Hashtable session = Sessions.Visitor;

            string strReferrer = Utility.GetDefaultValue(request.QueryString, "r", "");
            if (strReferrer.Length == 0)
            {
                strReferrer = Utility.GetDefaultValue(request.QueryString, "Sourceid", "");
            }
            string strSubID = Utility.GetDefaultValue(request.QueryString, "subid", "");
            string strUrlReferrer = request.UrlReferrer == null ? "" : request.UrlReferrer.ToString();
            int styleId = Convert.ToInt32(CampaignData["StyleID"]);
            int campaignId = Convert.ToInt32(CampaignData["CampaignID"]);
            //int landingPageOfferId = Convert.ToInt32(CampaignData["LandingPageOfferID"]);
            int landingPageOfferId = GetRandomLandingPage(campaignId);
            session["SessionGUID"] = Guid.NewGuid().ToString();
            session["ClientID"] = Convert.ToInt32(CampaignData["ClientID"]);
            //session["Platform"] = Convert.ToInt32(CampaignData["Platform"]);
            session["WebsiteID"] = Convert.ToInt32(CampaignData["WebsiteID"]);
            session["PublisherID"] = Convert.ToInt32(CampaignData["PublisherID"]);
            session["CampaignID"] = campaignId;
            HttpContext.Current.Session["CampaignID"] = campaignId;
            session["StyleID"] = styleId;
            session["LandingPageOfferID"] = landingPageOfferId;
            session["LandingPageCompleted"] = false;
            session["FinalPageOfferID"] = Convert.ToInt32(CampaignData["FinalPageOfferID"]);
            session["FlowID"] = int.MinValue;
            session["FlowIsStatic"] = true;
            session["DynamicFlowIsActive"] = Convert.ToBoolean(CampaignData["DynamicFlowIsActive"]);
            session["DynamicFlowMaxOffers"] = Convert.ToInt32(CampaignData["DynamicFlowMaxOffers"]);
            session["DynamicFlowWeight"] = Convert.ToInt32(CampaignData["DynamicFlowWeight"]);
            session["WCPNumberOfEntries"] = 0;  // counter
            session["OfferIndex"] = 0;  // counter
            session["CurrentOfferID"] = int.MinValue;
            session["PreviousOfferID"] = int.MinValue;
            session["Referrer"] = strReferrer;
            session["SubID"] = strSubID;

            //var forwardedFor = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            //string IpAddress = String.IsNullOrWhiteSpace(forwardedFor) ? HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] : forwardedFor.Split(',').Select(s => s.Trim()).First();
            string IpAddress = HttpContext.Current.Request.UserHostAddress;
            string strLocationCode = ros.GetLocationCodeByIPNumber(Utility.GetIPNumber(IpAddress));
            if (strLocationCode == "??")
                strLocationCode = ConfigurationManager.AppSettings["DefaultLocationCode"];
            session["LocationCode"] = strLocationCode;


            // 10.03.2012 - ReIniting the Lead Hastable from the Database
            //session["Lead"] = new Hashtable();

            ros.RefillLeadHashtable(campaignId,SessionID);


            session["OfferLead"] = new Hashtable();

            int intSessionID = SessionID;
            session["SessionID"] = SessionID;

            HttpContext.Current.Session["SessionID"] = intSessionID;
            SelectFlow();

            DataRow row = ros.Flow_GetStyleDetails(styleId);
            session["Style_BackgroundColor"] = row["BackgroundColor"].ToString();
            session["Style_BackgroundImage"] = row["BackgroundImage"].ToString();
            session["Style_HeaderWidth"] = Convert.ToInt32(row["HeaderWidth"]);
            session["Style_HeaderImagePath"] = row["HeaderImagePath"].ToString();
            session["Style_SkipButtonImagePath"] = row["SkipButtonImagePath"].ToString();
            session["Style_YesButtonImagePath"] = row["YesButtonImagePath"].ToString();
            session["Style_NoButtonImagePath"] = row["NoButtonImagePath"].ToString();
            // create visitor cookie if required & save to database
            InitializeVisitorCookie(request, response, campaignId, landingPageOfferId);
        }

        //private static void InitializeSession(int ClientID, int WebsiteID, int CampaignID, int StyleID, int LandingPageOfferID, int FinalPageOfferID)
        private static void InitializeSession(DataRow CampaignData, bool allowRedirect, bool clearSession = false)
        {
            HttpContext context = HttpContext.Current;
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;
            DataRow row42;
            int landingPageOfferId = -1;
            int campaignId = -1;

            // init visitor session only (session data is stored in a hashtable separate from the UI session)
            try
            {
                Sessions.InitVisitorSession();
                Hashtable session = Sessions.Visitor;

                string strReferrer = Utility.GetDefaultValue(request.QueryString, "r", "");
                string strSubID = Utility.GetDefaultValue(request.QueryString, "subid", "");
                string strReferID;
                if (HttpContext.Current.Session["RefererID"] != null && Convert.ToString(HttpContext.Current.Session["RefererID"]).Length > 0)
                    strReferID = Convert.ToString(HttpContext.Current.Session["RefererID"]);
                else
                    strReferID = String.Empty;
                //= Utility.GetDefaultValue(request.QueryString, "referid", "");
                string strUrlReferrer = request.UrlReferrer == null ? "" : request.UrlReferrer.ToString();
                int styleId = Convert.ToInt32(CampaignData["StyleID"]);
                campaignId = Convert.ToInt32(CampaignData["CampaignID"]);
                //int landingPageOfferId = Convert.ToInt32(CampaignData["LandingPageOfferID"]);
                landingPageOfferId = GetRandomLandingPage(campaignId);
                if (landingPageOfferId < 0)
                    throw new Exception("No landing page offerid found");
                session["SessionGUID"] = Guid.NewGuid().ToString();
                session["ClientID"] = Convert.ToInt32(CampaignData["ClientID"]);
                session["WebsiteID"] = Convert.ToInt32(CampaignData["WebsiteID"]);
                session["CampaignID"] = campaignId;
                session["StyleID"] = styleId;
                session["LandingPageOfferID"] = landingPageOfferId;
                session["LandingPageCompleted"] = false;
                session["FinalPageOfferID"] = Convert.ToInt32(CampaignData["FinalPageOfferID"]);
                session["FlowID"] = int.MinValue;
                session["FlowIsStatic"] = true;
                session["DynamicFlowIsActive"] = Convert.ToBoolean(CampaignData["DynamicFlowIsActive"]);
                session["DynamicFlowMaxOffers"] = Convert.ToInt32(CampaignData["DynamicFlowMaxOffers"]);
                session["DynamicFlowWeight"] = Convert.ToInt32(CampaignData["DynamicFlowWeight"]);
                session["WCPNumberOfEntries"] = 0;  // counter
                session["OfferIndex"] = 0;  // counter
                session["CurrentOfferID"] = int.MinValue;
                session["PreviousOfferID"] = int.MinValue;
                session["Referrer"] = strReferrer;
                session["SubID"] = strSubID;

                //var forwardedFor = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                //string IpAddress = String.IsNullOrWhiteSpace(forwardedFor) ? HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] : forwardedFor.Split(',').Select(s => s.Trim()).First();
                string IpAddress = HttpContext.Current.Request.UserHostAddress;

                string strLocationCode = ros.GetLocationCodeByIPNumber(Utility.GetIPNumber(IpAddress));
                if (strLocationCode == "??")
                    strLocationCode = ConfigurationManager.AppSettings["DefaultLocationCode"];
                session["LocationCode"] = strLocationCode;

                session["Lead"] = new Hashtable();
                session["OfferLead"] = new Hashtable();

                DataRow row = ros.Flow_GetStyleDetails(styleId);
                session["Style_BackgroundColor"] = row["BackgroundColor"].ToString();
                session["Style_HeaderWidth"] = Convert.ToInt32(row["HeaderWidth"]);
                session["Style_HeaderImagePath"] = row["HeaderImagePath"].ToString();
                session["Style_BackgroundImage"] = row["BackgroundImage"].ToString();
                session["Style_SkipButtonImagePath"] = row["SkipButtonImagePath"].ToString();
                session["Style_YesButtonImagePath"] = row["YesButtonImagePath"].ToString();
                session["Style_NoButtonImagePath"] = row["NoButtonImagePath"].ToString();
                // create visitor cookie if required & save to database
                InitializeVisitorCookie(request, response, campaignId, landingPageOfferId);

                int intSessionID = ros.Flow_InsertSession(
                    session["SessionGUID"].ToString(), (int)session["VisitorID"],
                    campaignId, strReferrer, strSubID,
                    request.RawUrl, request.UserAgent, strUrlReferrer, IpAddress,
                    session["LocationCode"].ToString(), strReferID, clearSession);

                ros.GAPC_InsertAffiliate(strReferID);

                string UserAgent = request.UserAgent;
                if (UserAgent != null && UserAgent.Contains("Mobile") == true)
                {
                    session["IsMobile"] = 1;
                }
                else
                {
                    session["IsMobile"] = 0;
                }

                session["SessionID"] = intSessionID;

                int intLPOfferImpressionID = ros.Flow_InsertOfferImpression(intSessionID, landingPageOfferId, int.MinValue, ImpressionProvider.LandingPage);
                session["OfferImpressionID"] = intLPOfferImpressionID;
                ros.Flow_UpdateSessionLP(intSessionID, intLPOfferImpressionID);
                session["CurrentOfferID"] = landingPageOfferId;

                row = ros.Flow_GetCampaignTrackingURLs(intSessionID);
                session["CampaignImpressionPixelURL"] = row["ImpressionURL"].ToString();
                session["CampaignConversionPixelURL"] = row["ConversionURL"].ToString();
                session["CampaignImpressionPixelFired"] = false;
                session["CampaignConversionPixelFired"] = false;
                row42 = ros.Flow_GetOfferDetails(campaignId, landingPageOfferId);
                if (row42 == null)
                    throw new Exception("Flow_GetOfferDetails has returned no rows");
            }
            catch (Exception ex)
            {
                ErrorHandling.SendException("InitializeSession", ex, "landingPageOfferId:" + landingPageOfferId.ToString() + "\ncampaignId:" + campaignId.ToString());
                row42 = null;
            }

            if (allowRedirect)
            {

                string r = String.Empty;
                string url = String.Empty;

                if (row42 != null)
                {
                    switch ((OfferType)Convert.ToInt32(row42["Type"]))
                    {
                        case OfferType.Internal:
                            url = "/hosting/landingpage.aspx?offerid=" + landingPageOfferId.ToString();
                            break;

                        case OfferType.Static:
                            DataRow row43 = ros.Flow_GetStaticFileName(landingPageOfferId);
                            url = "/hosting/staticpages/" + row43["FileName"].ToString();
                            break;
                    }

                    if (url.Length > 0)
                    {
                        if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["r"]))
                        {
                            if (url.Contains("?"))
                                url += "&r=";
                            else
                                url += "?r=";
                            url += HttpContext.Current.Request.QueryString["r"];
                        }
                        BustFrame(url, true);
                    }
                }
            }
            else
            {
                //if (HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] != "127.0.0.1")
                    //ErrorHandling.SendException("InitializeSession Not Allowed allowRedirect", new Exception(), "Current URL:" + HttpContext.Current.Request.Url.AbsoluteUri + "\nReferer:" + HttpContext.Current.Request.UrlReferrer);
            }
        }

        public static void InitializeSessionManual(string CampaignCode)
        {
            DataRow CampaignData = ros.Flow_GetCampaignDetailsByCode(CampaignCode);

            HttpContext context = HttpContext.Current;
            HttpRequest request = context.Request;
            HttpResponse response = context.Response;

            // init visitor session only (session data is stored in a hashtable separate from the UI session)
            Sessions.InitVisitorSession();
            Hashtable session = Sessions.Visitor;

            string strAffID = Utility.GetDefaultValue(request.QueryString, "affid", "");
            string strHitID = Utility.GetDefaultValue(request.QueryString, "hitid", "");
            string strC1 = Utility.GetDefaultValue(request.QueryString, "c1", "");
            string strC2 = Utility.GetDefaultValue(request.QueryString, "c2", "");
            string strC3 = Utility.GetDefaultValue(request.QueryString, "c3", "");
            string strReferID = Utility.GetDefaultValue(request.QueryString, "referid", "");

            session["C1Value"] = strC1;
            session["HitIDValue"] = strHitID;

            string strReferrer = Utility.GetDefaultValue(request.QueryString, "r", "");
            string strSubID = Utility.GetDefaultValue(request.QueryString, "subid", "");
            string strUrlReferrer = request.UrlReferrer == null ? "" : request.UrlReferrer.ToString();
            int styleId = Convert.ToInt32(CampaignData["StyleID"]);
            int campaignId = Convert.ToInt32(CampaignData["CampaignID"]);
            //int landingPageOfferId = Convert.ToInt32(CampaignData["LandingPageOfferID"]);
            int landingPageOfferId = GetRandomLandingPage(campaignId);
            session["SessionGUID"] = Guid.NewGuid().ToString();
            session["ClientID"] = Convert.ToInt32(CampaignData["ClientID"]);
            session["WebsiteID"] = Convert.ToInt32(CampaignData["WebsiteID"]);
            session["CampaignID"] = campaignId;
            session["StyleID"] = styleId;
            session["LandingPageOfferID"] = landingPageOfferId;
            session["LandingPageCompleted"] = false;
            session["FinalPageOfferID"] = Convert.ToInt32(CampaignData["FinalPageOfferID"]);
            session["FlowID"] = int.MinValue;
            session["FlowIsStatic"] = true;
            session["DynamicFlowIsActive"] = Convert.ToBoolean(CampaignData["DynamicFlowIsActive"]);
            session["DynamicFlowMaxOffers"] = Convert.ToInt32(CampaignData["DynamicFlowMaxOffers"]);
            session["DynamicFlowWeight"] = Convert.ToInt32(CampaignData["DynamicFlowWeight"]);
            session["WCPNumberOfEntries"] = 0;  // counter
            session["OfferIndex"] = 0;  // counter
            session["CurrentOfferID"] = int.MinValue;
            session["PreviousOfferID"] = int.MinValue;
            session["Referrer"] = strReferrer;
            session["SubID"] = strSubID;
            //var forwardedFor = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            //string IpAddress = String.IsNullOrWhiteSpace(forwardedFor) ? HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"] : forwardedFor.Split(',').Select(s => s.Trim()).First();
            string IpAddress = HttpContext.Current.Request.UserHostAddress;

            string strLocationCode = ros.GetLocationCodeByIPNumber(Utility.GetIPNumber(IpAddress));
            if (strLocationCode == "??")
                strLocationCode = ConfigurationManager.AppSettings["DefaultLocationCode"];
            session["LocationCode"] = strLocationCode;

            session["Lead"] = new Hashtable();
            session["OfferLead"] = new Hashtable();

            DataRow row = ros.Flow_GetStyleDetails(styleId);
            session["Style_BackgroundColor"] = row["BackgroundColor"].ToString();
            session["Style_BackgroundImage"] = row["BackgroundImage"].ToString();
            session["Style_HeaderWidth"] = Convert.ToInt32(row["HeaderWidth"]);
            session["Style_HeaderImagePath"] = row["HeaderImagePath"].ToString();
            session["Style_SkipButtonImagePath"] = row["SkipButtonImagePath"].ToString();
            session["Style_YesButtonImagePath"] = row["YesButtonImagePath"].ToString();
            session["Style_NoButtonImagePath"] = row["NoButtonImagePath"].ToString();
            // create visitor cookie if required & save to database
            InitializeVisitorCookie(request, response, campaignId, landingPageOfferId);

            string _ReturnPath = Utility.GetDefaultValue(request.QueryString, "rpath", "");
            //if (session["rpath"] != null)
            //{
            //    _ReturnPath = session["rpath"].ToString();
            //}

            int intSessionID = ros.Flow_InsertSession(
                session["SessionGUID"].ToString(), (int)session["VisitorID"],
                campaignId, strReferrer, strSubID,
                request.RawUrl, request.UserAgent, strUrlReferrer, IpAddress,
                session["LocationCode"].ToString(), _ReturnPath, strAffID, strHitID, strC1, strC2, strC3, strReferID);

            session["SessionID"] = intSessionID;
            string UserAgent = request.UserAgent;
            if (UserAgent != null && UserAgent.Contains("Mobile") == true)
            {
                session["IsMobile"] = 1;
            }
            else
            {
                session["IsMobile"] = 0;
            }

            int intLPOfferImpressionID = ros.Flow_InsertOfferImpression(intSessionID, landingPageOfferId, int.MinValue, ImpressionProvider.LandingPage);
            session["OfferImpressionID"] = intLPOfferImpressionID;
            ros.Flow_UpdateSessionLP(intSessionID, intLPOfferImpressionID);
            session["CurrentOfferID"] = landingPageOfferId;

            row = ros.Flow_GetCampaignTrackingURLs(intSessionID);
            session["CampaignImpressionPixelURL"] = row["ImpressionURL"].ToString();
            session["CampaignConversionPixelURL"] = row["ConversionURL"].ToString();
            session["CampaignImpressionPixelFired"] = false;
            session["CampaignConversionPixelFired"] = false;
            /*
            DataRow row42 = ros.Flow_GetOfferDetails(campaignId, landingPageOfferId);

            switch ((OfferType)Convert.ToInt32(row42["Type"]))
            {
                case OfferType.Internal:
                    BustFrame("landingpage.aspx?offerid=" + landingPageOfferId.ToString());
                    break;

                case OfferType.Static:
                    DataRow row43 = ros.Flow_GetStaticFileName(landingPageOfferId);
                    BustFrame("staticpages/" + row43["FileName"].ToString());
                    break;
            }
            */

        }

        public static int GetRandomLandingPage(int campaignId)
        {
            // returns offerid
            DataTable dt = ros.Flow_GetCampaignLandingPages(campaignId);
            Random r = new Random(DateTime.Now.Millisecond);

            return Convert.ToInt32(dt.Rows[r.Next(dt.Rows.Count)]["OfferID"]);
        }

        private static void InitializeVisitorCookie(HttpRequest request, HttpResponse response, int campaignId, int lpOfferId)
        {
            // [x-TESTED: OK]
            HttpCookie requestCookie = request.Cookies["campaign" + campaignId.ToString() + "-" + lpOfferId.ToString()];
            if (requestCookie == null || requestCookie["id"] == null || requestCookie["guid"] == null)
            {
                string visitorGUID = Utility.GetBase32UID();

                int visitorId = ros.Flow_InsertVisitor(campaignId, lpOfferId, visitorGUID);
                Sessions.Visitor["VisitorID"] = visitorId;

                HttpCookie responseCookie = response.Cookies["campaign" + campaignId.ToString() + "-" + lpOfferId.ToString()];
                responseCookie["id"] = visitorId.ToString();
                responseCookie["guid"] = visitorGUID;
                responseCookie.Expires = DateTime.Now.AddHours(24);
            }
            else
            {
                int visitorId = int.Parse(requestCookie["id"]);
                string visitorGUID = requestCookie["guid"];

                if (ros.Flow_IsVisitorValid(visitorId, campaignId, lpOfferId, visitorGUID))
                {
                    Sessions.Visitor["VisitorID"] = visitorId;
                }
            }
        }


        private static void SelectFlow()
        {
            // [TESTED: OK]
            // :: exceptions are uncaught; if no flow can be selected, an exception is thrown

            Hashtable session = Sessions.Visitor;

            int intCampaignID = (int)session["CampaignID"];
            int intSessionID = (int)session["SessionID"];
            int CurrentOfferID = (int)session["CurrentOfferID"];
            int intFlowID = 0;
            string aff = Functions.GetAffiliateName().ToLower();
            //if (intCampaignID == 3 && (aff.StartsWith("amped")))
            //{
            //    intFlowID = 15;
            //}
            //else if (intCampaignID == 3 && (aff == "h2h interactive"))
            //if (intCampaignID == 3 && (aff == "h2h interactive"))
            //{
            //    intFlowID = 25;
            //}
            //if (intCampaignID == 2 && (aff == "flu" || aff == "flu_"))
            //{
            //    intFlowID = 13;
            //}
            if (intCampaignID == 15 && CurrentOfferID == 58 && HttpContext.Current.Session["cleanFlow"] != null)
            {
                intFlowID = 30;
            }
            else if (intCampaignID == 18 && CurrentOfferID == 129 && HttpContext.Current.Session["cleanFlow"] != null)
            {
                intFlowID = 31;
            }
            else
            {
                DataTable dt = ros.Flow_GetCampaignFlows(intCampaignID, intSessionID);
                foreach (DataRow row in dt.Rows)
                {
                    intFlowID = Convert.ToInt32(row["FlowID"]);
                }
            }
            bool bIsDynamic = intFlowID == int.MinValue;

            session["FlowID"] = intFlowID;
            session["FlowIsStatic"] = !bIsDynamic;

            ros.Flow_UpdateSession((int)session["SessionID"], (int)session["FlowID"], bIsDynamic);
        }

        public static void UpdateLead()
        {
            if (Sessions.Visitor == null)
                Sessions.InitVisitorSession();

            // save landing-page lead (Lead) or offer lead (OfferLead)
            Hashtable session = Sessions.Visitor;
            if ((bool)session["LandingPageCompleted"])
            {
                try
                {

                    ros.Flow_SaveLead((int)session["OfferImpressionID"], (Hashtable)session["OfferLead"]);
                }
                catch { }
            }
            else
            {
                ros.Flow_SaveLead((int)session["OfferImpressionID"], (Hashtable)session["Lead"]);
            }
        }

        public static void RenderCampaignPixel(HtmlImage Image)
        {
            Hashtable session = Sessions.Visitor;

            if (!(bool)session["LandingPageCompleted"])
            {
                // landing page: show impression pixel if not done yet
                if ((bool)session["CampaignImpressionPixelFired"])
                    Image.Visible = false;
                else
                {
                    string strUrl = session["CampaignImpressionPixelURL"].ToString();
                    if (strUrl.Length == 0)
                        Image.Visible = false;
                    else
                    {
                        Image.Visible = true;
                        Image.Src = strUrl;
                        session["CampaignImpressionPixelFired"] = true;
                    }
                }
            }
            else
            {
                // offer page: show conversion pixel if not done yet
                if ((bool)session["CampaignConversionPixelFired"])
                    Image.Visible = false;
                else
                {
                    string strUrl = session["CampaignConversionPixelURL"].ToString();
                    if (strUrl.Length == 0)
                        Image.Visible = false;
                    else
                    {
                        Image.Visible = true;
                        Image.Src = strUrl;
                        session["CampaignConversionPixelFired"] = true;
                    }
                }
            }
        }

        public static string CompleteLandingPageWithReturn()
        {
            Hashtable session = Sessions.Visitor;
            Hashtable lead = (Hashtable)session["Lead"];
            bool existsSurveyPage = false;
            int SurveyPageID;

            session["LandingPageCompleted"] = true;
            session["leadid"] = ros.Flow_SaveLead((int)session["OfferImpressionID"], lead);

            NormalizeData(lead);
            if (((short)lead["_NormalAge"]) > 0 || ((string)lead["_NormalGender"]).Length > 0 || ((int)lead["_NormalIncome"]) > 0)
            {
                ros.Flow_UpdateLeadNormals((int)session["OfferImpressionID"],
                    (short)lead["_NormalAge"], (string)lead["_NormalGender"], (int)lead["_NormalIncome"]);
            }

            ros.Flow_FlagOfferImpressionAsCompleted((int)session["OfferImpressionID"]);
            Feed.EngageFeeds((int)session["OfferImpressionID"]);
            SelectFlow();
            if (!(bool)session["FlowIsStatic"])
            {
                DecisionEngine.CompileInitialScores();
            }

            return PresentNextOfferWithReturn();
        }

        public static string PresentNextOfferWithReturn()
        {
            if (Sessions.Visitor == null)
                Sessions.InitVisitorSession();

            Hashtable session = Sessions.Visitor;

            // clear offer lead data
            session["OfferLead"] = new Hashtable();

            // update PreviousOfferID BEFORE calling GetNextOffer; required by DecisionEngine
            session["PreviousOfferID"] = session["CurrentOfferID"];

            ImpressionProvider provider = ImpressionProvider.Unspecified;
            DataRow row = null;
            if ((bool)session["FlowIsStatic"] || (int)session["OfferIndex"] < (int)session["DynamicFlowMaxOffers"])
                row = GetNextOffer(out provider);

            if (row == null)
            {
                provider = ImpressionProvider.ConfirmationPage;
                int intFinalPageOfferID = (int)session["FinalPageOfferID"];

                session["OfferIndex"] = (int)session["OfferIndex"] + 1;
                session["CurrentOfferID"] = intFinalPageOfferID;

                session["OfferImpressionID"] = ros.Flow_InsertOfferImpression(
                    (int)session["SessionID"], intFinalPageOfferID, (int)session["PreviousOfferID"], provider);

                ros.Flow_UpdateSessionFinalPage((int)session["SessionID"], (int)session["OfferImpressionID"]);

                DataRow rowCheckStatic = ros.Flow_GetOfferDetails((int)session["CampaignID"], (int)session["PreviousOfferID"]);

                return BustFrameWithRedirect("/hosting/complete.aspx");
            }
            else
            {
                int intOfferID = Convert.ToInt32(row["OfferID"]);

                session["OfferIndex"] = (int)session["OfferIndex"] + 1; // counter; used by GetNextOffer()
                session["FollowUpOfferID"] = row["FollowUpOfferID"] != DBNull.Value ? Convert.ToInt32(row["FollowUpOfferID"].ToString()) : 0;
                session["CurrentOfferID"] = intOfferID;
                session["OfferImpressionID"] = ros.Flow_InsertOfferImpression(
                    (int)session["SessionID"], intOfferID, (int)session["PreviousOfferID"], provider);

                return BustFrameWithRedirect("/hosting/offer.aspx?offerid=" + intOfferID.ToString());
            }
        }
        public static string BustFrameWithRedirect(string URL)
        {
            HttpRequest Request = HttpContext.Current.Request;

            bool isPostBack = false;
            Page pageHandler = HttpContext.Current.CurrentHandler as Page;
            if (pageHandler != null)
                isPostBack = pageHandler.IsPostBack;

            Hashtable session = Sessions.Visitor;
            DataRow rowCheckStatic = ros.Flow_GetOfferDetails((int)session["CampaignID"], (int)session["PreviousOfferID"]);

            StringBuilder sb = new StringBuilder();

            foreach (String key in Request.QueryString.AllKeys)
            {
                if (!URL.Replace('?', '&').Contains("&" + key + "="))
                {
                    if (sb.Length == 0 && !URL.Contains("?"))
                        sb.Append("?");
                    else
                        sb.Append("&");
                    sb.Append(key).Append("=").Append(HttpUtility.UrlEncode(Request.QueryString[key]));
                }
            }

            URL += sb.ToString();

            return URL;
        }


        #region // Session/lead data //
        public static int CurrentClientID
        {
            get { return (int)Sessions.Visitor["ClientID"]; }
        }

        public static Hashtable CurrentLead
        {
            get
            {
                int id;
                Hashtable lead = null;
                if (Sessions.Visitor != null)
                    lead = (Hashtable)Sessions.Visitor["Lead"];
                else if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString.Get("recordid")) && int.TryParse(HttpContext.Current.Request.QueryString.Get("recordid"), out id))
                {
                    ros.RefillLeadHashtable(id);
                    lead = (Hashtable)Sessions.Visitor["Lead"];
                }
                else
                {
                    if (Sessions.Visitor == null)
                        Sessions.InitVisitorSession();
                    lead = new Hashtable();
                    Sessions.Visitor["Lead"] = lead;
                }

                try
                {
                    if ((!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["email"])) && (lead["emailaddress"] == null || Convert.ToString(lead["emailaddress"]).Length == 0) && HttpContext.Current.Request.QueryString["email"].Contains("@"))
                        lead["emailaddress"] = HttpContext.Current.Request.QueryString["email"];
                    if ((!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["firstname"])) && (lead["firstname"] == null || Convert.ToString(lead["firstname"]).Length == 0))
                        lead["firstname"] = HttpContext.Current.Request.QueryString["firstname"];
                    if ((!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["lastname"])) && (lead["lastname"] == null || Convert.ToString(lead["lastname"]).Length == 0))
                        lead["lastname"] = HttpContext.Current.Request.QueryString["lastname"];
                    if ((!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["email"])) && (lead["emailaddress"] == null || Convert.ToString(lead["emailaddress"]).Length == 0))
                        lead["emailaddress"] = HttpContext.Current.Request.QueryString["email"];
                    if ((!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["phone"])) && (lead["homephoneno"] == null || Convert.ToString(lead["homephoneno"]).Length == 0))
                        lead["homephoneno"] = HttpContext.Current.Request.QueryString["phone"];
                    if ((!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["address"])) && (lead["address"] == null || Convert.ToString(lead["address"]).Length == 0))
                        lead["address"] = HttpContext.Current.Request.QueryString["address"];
                    if ((!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["city"])) && (lead["city"] == null || Convert.ToString(lead["city"]).Length == 0))
                        lead["city"] = HttpContext.Current.Request.QueryString["city"];
                    if ((!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["state"])) && (lead["state"] == null || Convert.ToString(lead["state"]).Length == 0))
                        lead["state"] = HttpContext.Current.Request.QueryString["state"];
                    if ((!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["zip"])) && (lead["postalcode"] == null || Convert.ToString(lead["postalcode"]).Length == 0))
                        lead["postalcode"] = HttpContext.Current.Request.QueryString["zip"];
                    if ((!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["dob"])) && (lead["DateOfBirth"] == null || Convert.ToString(lead["DateOfBirth"]).Length == 0))
                        lead["DateOfBirth"] = HttpContext.Current.Request.QueryString["dob"];
                    // hashtable keys are case-sensitive, and most of the site looks for the lowercase version
                    if ((!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["dob"])) && (lead["dateofbirth"] == null || Convert.ToString(lead["dateofbirth"]).Length == 0))
                        lead["dateofbirth"] = HttpContext.Current.Request.QueryString["dob"];
                    if (!String.IsNullOrEmpty(HttpContext.Current.Request.QueryString["referID"]))
                        HttpContext.Current.Session["RefererID"] = HttpContext.Current.Request.QueryString["referID"];
                }
                catch (Exception ex)
                {
                    ErrorHandling.SendException("Hashtable CurrentLead in QueryString", ex);
                }

                PullC4RLeadFromDB(lead);       

                return lead;
            }
        }

        public static void PullLeadFromC4RAndGAPC(Hashtable lead, string customTimeoutField = null)
        {
            PullC4RLeadFromDB(lead);
            Functions.RegDataCompletionLevel regDataCompletion = Functions.CheckRegDataCompletion(lead);
            if (regDataCompletion != Functions.RegDataCompletionLevel.Full)
            {
                Functions.PullGAPCDirectSaveIntoLead(customTimeoutField: customTimeoutField);
                // re-check data completion after gapc lookup
                regDataCompletion = Functions.CheckRegDataCompletion(lead);
            }
            else
            {
                if (customTimeoutField != null && lead[customTimeoutField] != null)
                {
                    lead["custom7"] = "firstname|lastname|homephoneno|address|city|state|postalcode|dateofbirth";
                }
            }
            lead["custom10"] = regDataCompletion.ToString().ToLower();
        }

        public static void PullC4RLeadFromDB(Hashtable lead)
        {
            if (HttpContext.Current.Session["TryGetUserDataFromDB"] == null && lead["emailaddress"] != null && !String.IsNullOrEmpty(Convert.ToString(lead["emailaddress"])) && Convert.ToString(lead["emailaddress"]) != "{email}")
            {
                try
                {
                    HttpContext.Current.Session["TryGetUserDataFromDB"] = true;

                    DataTable dt3 = new DataTable();
                    DBCON aDB3 = new DBCON();

                    aDB3.SqlCommand.CommandText = "Flow_CheckEmail";
                    aDB3.SqlCommand.Parameters.AddWithValue("@email", lead["emailaddress"]);

                    dt3 = aDB3.GetDataTable();

                    if (dt3.Rows.Count > 0)
                    {
                        if (dt3.Rows[0]["FirstName"] != DBNull.Value && (lead["firstname"] == null || Convert.ToString(lead["firstname"]).Length == 0))
                            lead["firstname"] = dt3.Rows[0]["FirstName"].ToString();
                        if (dt3.Rows[0]["LastName"] != DBNull.Value && (lead["lastname"] == null || Convert.ToString(lead["lastname"]).Length == 0))
                            lead["lastname"] = dt3.Rows[0]["LastName"].ToString();
                        if (dt3.Rows[0]["Phone"] != DBNull.Value && (lead["homephoneno"] == null || Convert.ToString(lead["homephoneno"]).Length == 0))
                            lead["homephoneno"] = dt3.Rows[0]["Phone"].ToString();
                        if (dt3.Rows[0]["Address"] != DBNull.Value && (lead["address"] == null || Convert.ToString(lead["address"]).Length == 0))
                            lead["address"] = dt3.Rows[0]["Address"].ToString();
                        if (dt3.Rows[0]["City"] != DBNull.Value && (lead["city"] == null || Convert.ToString(lead["city"]).Length == 0))
                            lead["city"] = dt3.Rows[0]["City"].ToString();
                        if (dt3.Rows[0]["State"] != DBNull.Value && (lead["state"] == null || Convert.ToString(lead["state"]).Length == 0))
                            lead["state"] = dt3.Rows[0]["State"].ToString();
                        if (dt3.Rows[0]["PostalCode"] != DBNull.Value && (lead["postalcode"] == null || Convert.ToString(lead["postalcode"]).Length == 0))
                            lead["postalcode"] = dt3.Rows[0]["PostalCode"].ToString();
                        if (dt3.Rows[0]["DOB"] != DBNull.Value && (lead["DateOfBirth"] == null || Convert.ToString(lead["DateOfBirth"]).Length == 0))
                            lead["DateOfBirth"] = dt3.Rows[0]["DOB"].ToString();
                        // hashtable keys are case-sensitive, and most of the site looks for the lowercase version
                        if (dt3.Rows[0]["DOB"] != DBNull.Value && (lead["dateofbirth"] == null || Convert.ToString(lead["dateofbirth"]).Length == 0))
                            lead["dateofbirth"] = dt3.Rows[0]["DOB"].ToString();
                    }

                }
                catch(Exception ex)
                {
                    ErrorHandling.SendException("Hashtable CurrentLead in Database", ex);
                }
                
            }
        }

        public static Hashtable CurrentOfferLead
        {
            get
            {
                return Sessions.Visitor == null
                    ? null : (Hashtable)Sessions.Visitor["OfferLead"];
            }
        }

        public static int CurrentSessionID
        {
            get
            {
                return Sessions.Visitor == null
                    ? int.MinValue : (int)Sessions.Visitor["SessionID"];
            }
        }

        public static string GetLeadValue(Hashtable Lead, string Field)
        {
            if (Lead == null)
                return "";

            return Lead[Field.ToLower()] == null ? "" : Lead[Field.ToLower()].ToString();
        }
        #endregion

    }
}