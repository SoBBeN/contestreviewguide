﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Net;

public partial class SweepContestsThumbnail : System.Web.UI.Page
{
    protected string sourcePath;
    protected string type;

    protected void Page_Init(object sender, EventArgs e)
    {
        sourcePath = System.Configuration.ConfigurationManager.AppSettings["baseimagessweepcontesturl"];
    }

    protected void Page_Load(object sender, EventArgs e)
    {
        string sourceFile = Request.QueryString["f"];

        imgCropbox.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagessweepcontesturl"];

        imgCropbox.Src += sourceFile + "?r=" + Guid.NewGuid().ToString();
        imgCropbox.Attributes.Add("onload", "updateDivWidth(this.width, this.height, this);");
    }


    // Source : http://stackoverflow.com/questions/2163829/how-do-i-rotate-a-picture-in-c-sharp


    protected void btnRotate_ClickLeft(object sender, EventArgs e)
    {
        Rotate(RotateFlipType.Rotate270FlipNone);
    }
    protected void btnRotate_ClickRight(object sender, EventArgs e)
    {
        Rotate(RotateFlipType.Rotate90FlipNone);
    }

    protected void Rotate(RotateFlipType angle)
    {
        string sourcePath = System.Configuration.ConfigurationManager.AppSettings["baseimagessweepcontesturl"];
        string ImageName = Request.QueryString["f"];

        byte[] CropImage = Rotate(Path.Combine(sourcePath, ImageName), angle);

        if (CropImage != null)
        {
            var azureStorage = new AzureStorage("AzureStorageConnection");
            var blob = azureStorage.UploadBlog("themommyguide", "images/sweepcontests", ref ImageName, CropImage, true);
        }
    }

    static byte[] Rotate(string Img, RotateFlipType angle)
    {
        WebClient wc = new WebClient();
        byte[] bytes = wc.DownloadData(Img);
        MemoryStream ms = new MemoryStream(bytes);

        using (System.Drawing.Image OriginalImage = System.Drawing.Image.FromStream(ms))
        {
            System.Drawing.Imaging.ImageFormat oldFormat = OriginalImage.RawFormat;
            OriginalImage.RotateFlip(angle);

            using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(OriginalImage.Width, OriginalImage.Height))
            {
                bmp.SetResolution(OriginalImage.HorizontalResolution,
                  OriginalImage.VerticalResolution);

                using (System.Drawing.Graphics Graphic = System.Drawing.Graphics.FromImage(bmp))
                {
                    Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                    Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    Graphic.DrawImage(OriginalImage, new Point(0, 0));
                    ms = new MemoryStream();
                    bmp.Save(ms, oldFormat);
                    return ms.GetBuffer();
                }
            }
        }
    }




    // Source : http://stackoverflow.com/questions/10446621/asp-net-jcrop-value-issue
    protected void btnCrop_Click(object sender, EventArgs e)
    {
        string sourceFile = Request.QueryString["f"];
        int imageid = Convert.ToInt32(Request.QueryString["id"]);

        //File.Move(Path.Combine(sourcePath, sourceFile), Path.Combine(sourcePath, destinationFile));

        string thumbnailFile;

        int w;
        int h;
        int x;
        int y;

        if (int.TryParse(W.Value, out w) && int.TryParse(H.Value, out h) && int.TryParse(X.Value, out x) && int.TryParse(Y.Value, out y))
        {
            if (String.IsNullOrEmpty(Request.QueryString["t"]))
                thumbnailFile = Functions.GetNonExistingFilename(sourcePath, "t" + sourceFile);
            else
                thumbnailFile = Request.QueryString["t"];

            byte[] CropImage = Crop(Path.Combine(sourcePath, sourceFile), w, h, x, y);

            if (CropImage != null)
            {
                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("themommyguide", "images/sweepcontests", ref thumbnailFile, CropImage, true);
            }
        }
        else
            thumbnailFile = null;

        DB.DbFunctions.SweepContestsImageUpdateThumbnail(imageid, thumbnailFile);
        Response.Redirect("SweepContestsAdd.aspx?id=" + imageid.ToString());
    }

    static byte[] Crop(string Img, int Width, int Height, int X, int Y)
    {
        WebClient wc = new WebClient();
        byte[] bytes = wc.DownloadData(Img);
        MemoryStream ms = new MemoryStream(bytes);

        using (System.Drawing.Image OriginalImage = System.Drawing.Image.FromStream(ms))
        {
            using (System.Drawing.Bitmap bmp = new System.Drawing.Bitmap(Width, Height))
            {
                bmp.SetResolution(OriginalImage.HorizontalResolution,
                  OriginalImage.VerticalResolution);

                using (System.Drawing.Graphics Graphic = System.Drawing.Graphics.FromImage(bmp))
                {
                    Graphic.SmoothingMode = SmoothingMode.AntiAlias;
                    Graphic.InterpolationMode = InterpolationMode.HighQualityBicubic;
                    Graphic.PixelOffsetMode = PixelOffsetMode.HighQuality;
                    Graphic.DrawImage(OriginalImage,
                      new System.Drawing.Rectangle(0, 0, Width, Height),
                      X, Y, Width, Height, System.Drawing.GraphicsUnit.Pixel);
                    ms = new MemoryStream();
                    bmp.Save(ms, OriginalImage.RawFormat);
                    return ms.GetBuffer();
                }
            }
        }
    }
}