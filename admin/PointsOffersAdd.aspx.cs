﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PointsOffersAdd : System.Web.UI.Page
{
    protected const string ITEMNAME = "Daily Deal";

    protected void Page_Load(object sender, EventArgs e)
    {
        divMsg.Visible = false;

        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null) //UPDATE MODE
            {
                hidID.Value = Request.QueryString["id"]; //Saving the ID to use later
                ShowExistingValues();
            }
            int id;
            if (!int.TryParse(hidID.Value, out id))
                id = int.MinValue;

            FillDdl(DB.DbFunctions.GetPointsOffersCategory(id), sltCategory);
        }
    }

    private void FillDdl(SqlDataReader dr, MyClassLibrary.Controls.Selectable slt)
    {
        if (dr != null)
        {
            if (dr.HasRows)
            {
                /*slt.DataSource = dr;
                slt.DataTextField = "Description";
                slt.DataValueField = "ID";
                slt.DataBind();*/
                slt.Items.Clear();
                while (dr.Read())
                {
                    ListItem i = new ListItem(Convert.ToString(dr["Description"]), Convert.ToString(dr["ID"]));
                    i.Selected = Convert.ToBoolean(dr["IsSelected"]);
                    slt.Items.Add(i);
                }
            }
            dr.Close();
        }
    }


    private void ShowExistingValues()
    {
        int id = int.Parse(hidID.Value);
        SqlDataReader dr = DB.DbFunctions.GetPointsOffers(id);

        if (dr.HasRows)
        {
            dr.Read();
            imgImageFile.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"] + Functions.ConvertToString(dr["ImageFilename"]);
            imgThumbnail.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"] + Functions.ConvertToString(dr["Thumbnail"]);
            txtTitle.Text = Functions.ConvertToString(dr["Title"]);
            txtText.Text = Functions.ConvertToString(dr["Text"]);
            txtName.Text = Functions.ConvertToString(dr["Name"]);
            txtLinkURL.Text = Functions.ConvertToString(dr["Url"]);
            txtPoints.Text = Functions.ConvertToString(dr["NbPoints"]);
            txtDayExpiry.Text = Functions.ConvertToString(dr["DateExpire"]);
            chkActiveAll.Checked = Convert.ToBoolean(dr["IsActiveAll"]);
            chkActive.Checked = Convert.ToBoolean(dr["IsActive"]);
            Simple3Des s = new Simple3Des();
            lnkLink.HRef = System.Configuration.ConfigurationManager.AppSettings["baseurl"] + "qpoints.ashx?oid=" + HttpUtility.UrlEncode(s.EncryptData(id.ToString())) + "&uid=[RecordID]";
            lnkLink.InnerHtml = lnkLink.HRef;
            btnSave.Text = "Update " + ITEMNAME;

            lnkPreview.HRef = System.Configuration.ConfigurationManager.AppSettings["basepreviewurl"]+ "DailyDeals/" + id + "/" + Functions.StrToURL(Functions.ConvertToString(dr["Title"])) + "/";
            lnkPreview.Visible = true;

            lnkThumbnail.HRef = "MomentsThumbnail.aspx?type=dd&id=" + hidID.Value + "&f=" + HttpUtility.UrlEncode(Functions.ConvertToString(dr["ImageFilename"]));
            if (dr["Thumbnail"] != DBNull.Value)
                lnkThumbnail.HRef += "&t=" + HttpUtility.UrlEncode(Functions.ConvertToString(dr["Thumbnail"]));
            lnkThumbnail.Disabled = false;

            lblActive.Text = "Active on " + System.Configuration.ConfigurationManager.AppSettings["Title"] + ": ";
            lblDay.Text = "Active Date on " + System.Configuration.ConfigurationManager.AppSettings["Title"] + ": ";

            if (dr.NextResult())
            {
                StringBuilder sb = new StringBuilder();
                StringBuilder sbWebSiteIDs = new StringBuilder();
                while (dr.Read())
                {
                    if (sbWebSiteIDs.Length > 0)
                        sbWebSiteIDs.Append(",");
                    sbWebSiteIDs.Append(Convert.ToString(dr["WebSiteID"]));

                    string fieldname = "ckb" + Convert.ToString(dr["WebSiteID"]);
                    string websitename = Convert.ToString(dr["WebSiteName"]);
                    string value;
                    if (Convert.ToBoolean(dr["IsActive"]))
                        value = "checked=\"checked\" ";
                    else
                        value = string.Empty;

                    sb.Append("<tr><td>&nbsp;</td><td><hr /></td></tr>");
                    sb.AppendFormat("<tr><td><label for=\"{0}\" id=\"lblfor{0}\">Active on {1}: </label></td>", fieldname, websitename);
                    sb.AppendFormat("<td><input id=\"{0}\" name=\"{0}\" type=\"checkbox\" {1}/></td></tr>", fieldname, value);

                    fieldname = "day" + Convert.ToString(dr["WebSiteID"]);
                    value = Functions.ConvertToString(dr["ActiveDate"]);
                    sb.AppendFormat("<tr><td style=\"white-space:nowrap\"><label for=\"{0}\" id=\"lblfor{0}\">Active Date on {1}:</label></td>", fieldname, websitename);
                    sb.AppendFormat("<td><input id=\"{0}\" name=\"{0}\" type=\"text\" maxlength=\"10\" size=\"10\" class=\"text\" value=\"{1}\" />", fieldname, value);
                    sb.Append("    <script type=\"text/javascript\">");
                    sb.Append("        $(function () {");
                    sb.Append("            $(\"#").Append(fieldname).Append("\").datepicker({");
                    sb.Append("                dateFormat: \"mm/dd/yy\",");
                    sb.Append("                changeMonth: true,");
                    sb.Append("                changeYear: true");
                    sb.Append("            });");
                    sb.Append("        });");
                    sb.Append("    </script></td></tr>");

                }
                sb.Append("<tr><td>&nbsp;</td><td><hr /></td></tr>");
                sb.AppendFormat("<input type=\"hidden\" id=\"hidMoreWebSiteList\" name=\"hidMoreWebSiteList\" value=\"{0}\" />", sbWebSiteIDs.ToString());
                litMoreWebSite.Text = sb.ToString();
            }
        }
        else //can't find the poll.. change to INSERT MODE
        {
            hidID.Value = String.Empty;
            lnkThumbnail.Disabled = true;
            btnSave.Text = "Save " + ITEMNAME;
        }
        dr.Close();
    }


    protected void btnSaveAdd_Click(object sender, EventArgs e)
    {
        btnSave_Click(sender, e);
        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            string filename = String.Empty;
            if (ImageFile.HasFile)
            {
                filename = Functions.RemoveSpecialChars(ImageFile.FileName);

                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("couponinsanity", "images/coupons", ref filename, ImageFile.PostedFile.InputStream);
            }

            string thumbnail = String.Empty;
            if (fileThumbnail.HasFile)
            {
                thumbnail = Functions.RemoveSpecialChars(fileThumbnail.FileName);

                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("couponinsanity", "images/coupons", ref thumbnail, fileThumbnail.PostedFile.InputStream);
            }

            int id = 0;

            if (hidID.Value.Length == 0) //INSERT
            {
                id = DB.DbFunctions.InsertPointsOffers(txtTitle.Text, txtLinkURL.Text, txtPoints.Text, txtDay.Text, txtDayExpiry.Text, chkActiveAll.Checked, chkActive.Checked, txtText.Text, filename, thumbnail, txtName.Text);

                //POLL INSERTED SUCCESSFULLY
                if (id > 0)
                {
                    hidID.Value = id.ToString();
                    divMsg.InnerHtml = "Your new " + ITEMNAME + " has been created successfully.";
                    divMsg.Visible = true;
                    ShowExistingValues();
                }
            }
            else //UPDATE
            {
                id = int.Parse(hidID.Value);
                DB.DbFunctions.UpdatePointsOffers(id, txtTitle.Text, txtLinkURL.Text, txtPoints.Text, txtDay.Text, txtDayExpiry.Text, chkActiveAll.Checked, chkActive.Checked, txtText.Text, filename, thumbnail, txtName.Text);

                divMsg.InnerHtml = "Your " + ITEMNAME + " has been updated successfully.";
                divMsg.Visible = true;
            }
            if (!String.IsNullOrEmpty(Request.Form["hidMoreWebSiteList"]))
            {
                string[] websites = Convert.ToString(Request.Form["hidMoreWebSiteList"]).Split(',');
                foreach (string websiteid in websites)
                {
                    DB.DbFunctions.UpdatePointsOffers(id, Convert.ToInt32(websiteid), Request.Form["ckb" + websiteid] != null && Request.Form["ckb" + websiteid] == "on", Request.Form["day" + websiteid]);
                }
            }
            if (id > 0)
            {
                foreach (ListItem item in sltCategory.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertPointsOffersCategory(id, Convert.ToInt32(item.Value));
                }
                ShowExistingValues();
            }
        }
    }

    protected void btnCategory_Click(object sender, EventArgs e)
    {
        DB.DbFunctions.PointsOffersInsertCategory(txtAddCat.Text);
        txtAddCat.Text = String.Empty;

        int id;
        if (!int.TryParse(hidID.Value, out id))
            id = int.MinValue;
        
        FillDdl(DB.DbFunctions.GetPointsOffersCategory(id), sltCategory);
    }
}