﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class MOMentsApproval : System.Web.UI.Page
{
    protected new SortedDictionary<int, string> imageTypes;

    protected void Page_Load(object sender, EventArgs e)
    {
        FillImgTypes();
    }

    private void FillImgTypes()
    {
        SqlDataReader dr = DB.DbFunctions.GetSlider();
        imageTypes = new SortedDictionary<int, string>();

        if (dr != null && dr.HasRows)
        {
            int i = 1;
            while (dr.Read())
            {
                imageTypes.Add(Convert.ToInt32(dr["TypeID"]), Convert.ToString(dr["Description"]));
            }
        }
        dr.Close();
    }

    private void LoadPhotos(int typeid, int photoid)
    {
        StringBuilder sb = new StringBuilder();

        SqlDataReader dr = null; // = DB.DbFunctions.GetPhotosPaging(typeid, 0, 0);

        bool ogdone = false;

        if (dr != null && dr.HasRows)
        {
            int i = 0;

            sb.Append("<div id=\"MOMents\">");
            while (dr.Read() && i++ < 9)
            {
                int id = Convert.ToInt32(dr["ID"]);
                sb.Append("<div class=\"MOMent\">");

                sb.Append("<div class=\"img\"><img src=\"" + System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "images/").Append(dr["ImageFilename"]).Append("\" /></div>\n");

                if (dr["Thumbnail"] == DBNull.Value)
                    sb.Append("<div class=\"thumbnail\">Not Selected</div>\n");
                else
                    sb.Append("<div class=\"thumbnail\"><img src=\"" + System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "images/").Append(dr["Thumbnail"]).Append("\" /></div>\n");

                sb.Append("<select ");

                sb.Append("</div>");

                string link = String.Format("<a href=\"javascript:void(0);\" onclick=\"OpenPhoto({0});\"", Convert.ToString(dr["ID"]));

                if (i == 1)
                {
                    //sbStyles.Append("#photos {\nbackground-color:#").Append(dr["Color"]).Append("; }");
                    //sbStyles.Append("#photos .photo {\nbackground-color:#").Append(dr["BColor"]).Append("; border: solid 5px #").Append(dr["Color"]).Append(" }");

                    //if (DBNull.Value == dr["TitleImg"])
                    //    litTitle.Text = Convert.ToString(dr["TypeDesc"]);
                    //else
                    //    litTitle.Text = "<img src=\"/images/" + Convert.ToString(dr["TitleImg"]) + "\" alt=\"" + Convert.ToString(dr["TypeDesc"]) + "\" />";

                    //if (DBNull.Value != dr["SubTitle"])
                    //    litSubTitle.Text = Convert.ToString(dr["SubTitle"]);

                    sb.Append("<input type=\"hidden\" id=\"firstid\" value=\"" + Convert.ToString(dr["ID"]) + "\" />");

                }


                if (i % 3 == 1)
                {
                    sb.Append("<tr>\n");
                }

                sb.Append("<td class=\"photo\">\n");
                sb.Append("<div class=\"top\" style=\"background-image:url('" + System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "images/").Append(Convert.ToString(dr["ImageFilename"]).Replace("'", "\\'")).Append("');\" /><div onclick=\"OpenPhoto(").Append(dr["ID"]).Append(")\" class=\"over\"></div></div>\n");

                string username;
                if (dr["OverrideUsername"] == DBNull.Value)
                {
                    username = Functions.ConvertToString(dr["Lastname"]);
                    if (username.Length > 1)
                        username = username.Substring(0, 1) + '.';
                    else if (username.Length == 1)
                        username += ".";
                    username = Functions.ConvertToString(dr["Firstname"]) + ' ' + username;
                }
                else
                {
                    username = Functions.ConvertToString(dr["OverrideUsername"]);
                }

                sb.Append("<div class=\"bottom\" onclick=\"OpenPhoto(").Append(dr["ID"]).Append(")\">Shared By <span>").Append(username).Append("</span> on <span>").Append(Convert.ToDateTime(dr["DateIN"]).ToString("MMM d")).Append("</span>\n");

                string imgsrc = System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "images/" + Convert.ToString(dr["ImageFilename"]);
                int type = Convert.ToInt32(dr["Type"]);
                string titleurl = "/Photos/" + type + "/" + Functions.StrToURL(Convert.ToString(dr["TypeDesc"])) + "/" + photoid + "/";

            }
            //litPhotos.Text = sb.ToString();
        }
        dr.Close();
    }

    [WebMethod]
    public static int UpdateMOMent(int id, int status, short category, string user, string text)
    {
        var userInfo = DB.DbFunctions.UpdateImage(id, category, status, text, user);

        if (userInfo != null)
        {
            DB.DbFunctions.UpdateImageC4R(int.Parse(userInfo["CampaignID"].ToString()), userInfo["Email"].ToString(), userInfo["FirstName"].ToString(), userInfo["LastName"].ToString(),
                                            userInfo["Phone"].ToString(), userInfo["Address"].ToString(), userInfo["City"].ToString(), userInfo["State"].ToString(), userInfo["Zip"].ToString(),
                                            userInfo["PhotoLink"].ToString(), int.Parse(userInfo["UserID"].ToString()));
        }

        return id;
    }
}