﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="LifestyleSlideAdd.aspx.cs" Inherits="LifestyleSlideAdd" ValidateRequest="false" %>

<%@ Register Assembly="MyClassLibrary" Namespace="MyClassLibrary.Controls" TagPrefix="CustomControl" %>

<asp:Content ID="Content3" ContentPlaceHolderID="head" Runat="Server">
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.0/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.0/themes/start/jquery-ui.css" />
    <script src="ckeditor446/ckeditor.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#<%=txtDay.ClientID %>").datepicker({
                dateFormat: "mm/dd/yy",
                changeMonth: true,
                changeYear: true
            });
        });
        /*
        CKEDITOR.config.toolbar = [
            { name: 'clipboard', items: ['Cut', 'Copy', 'Paste', 'PasteText', "PasteFromWord", 'Undo', 'Redo', "Scayt"] },
            { name: 'font', items: ["NumberedList", "BulletedList", "CreateDiv", "Outdent", "Indent", "JustifyLeft", "JustifyCenter", "JustifyRight", "JustifyBlock"] },
            { name: 'html', items: ["Link", "UnLink", "Image", "Flash", "HorizontalRule", "SpecialChar", "-"] },
            "/",
            { name: 'text', items: ['Bold', 'Italic', 'Underline', "TextColor", "BGColor", "RemoveFormat"] },
            { name: 'style', items: ['Styles', 'Format', 'Font', 'FontSize', 'lineheight'] },
            { name: 'Maximize', items: ["Maximize"] },
            { name: 'Source', items: ['Source'] },
            { name: 'Source', items: ["About"] }
        ];*/

    </script>
	<style>
	  .selectable .ui-selecting { background: #FECA40; }
	  .selectable .ui-selected { background: #F39814; color: white; }
	  .selectable { list-style-type: none; margin: 0; padding: 0; width: 100%; }
	  .selectable li { margin: 3px; padding: 1px; float: left; width: 183px; height: 25px; font-size: 18px; font-weight:bold; line-height:25px; text-align: center; }
	</style>

</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <h3>Add/Edit <%=ITEMNAME %></h3>
    <br />
    <div runat="server" id="divMsg" class="mInfo" visible="false">
    </div>
    <div class="form" id="form">
        <div>
            <img runat="server" id="imgImageFile" style="max-height:170px; max-width:350px;" />
            <img runat="server" id="imgThumbnail" style="max-height:170px; max-width:350px;" />
        </div><br />
        <table width="100%">
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblArticle" AssociatedControlID="ddlArticle" Text="Article:" />
                </td>
                <td>
                    <asp:DropDownList runat="server" ID="ddlArticle" Width="280px">
                    </asp:DropDownList> <a href="javascript:void(0)" onclick="window.location = 'LifestyleAdd.aspx?id=' + document.getElementById('<%=ddlArticle.ClientID %>').options[document.getElementById('<%=ddlArticle.ClientID %>').selectedIndex].value;">Go To Article</a>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblImageFile" AssociatedControlID="ImageFile" Text="Image File:" />
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="ImageFile" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblThumbnail" AssociatedControlID="ImageFile" Text="Thumbnail File:" />
                </td>
                <td>
                    <asp:FileUpload runat="server" ID="fileThumbnail" /> <a runat="server" id="lnkThumbnail">Edit from Image</a>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblImageFileVisible" Text="Show Image File:" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkImageVisible" Checked="true" />
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblSubTitle" AssociatedControlID="txtSubtitle" Text="Slide Title:" />
                </td>
                <td>
                    <asp:TextBox ID="txtSubtitle" runat="server" CssClass="text" Columns="100" Width="350px" MaxLength="150" />&nbsp;
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblDescriptionTop" AssociatedControlID="txtDescriptionTop" Text="Before Image:" />
                </td>
                <td>
                    <asp:TextBox TextMode="MultiLine" ID="txtDescriptionTop" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtDescriptionTop.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>

                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblDescription" AssociatedControlID="txtDescription" Text="After Image:" />
                </td>
                <td>
                    <br />
                    <asp:TextBox TextMode="MultiLine" ID="txtDescription" runat="server" Width="800px" Height="300px"></asp:TextBox>
                    <script>
                        var roxyFileman = 'fileman/index.html';
                        $(function () {
                            CKEDITOR.replace('<%=txtDescription.ClientID %>', {
                                filebrowserBrowseUrl: roxyFileman,
                                filebrowserImageBrowseUrl: roxyFileman + '?type=image',
                                removeDialogTabs: 'link:upload;image:upload'
                            });
                        });
                    </script>
                    <br />
                </td>
            </tr>
            <tr>
                <td style="vertical-align:middle;">
                    <asp:Label runat="server" ID="lblVideo" AssociatedControlID="txtVideo" Text="Video Script:" />
                </td>
                <td>
                    <asp:TextBox ID="txtVideo" runat="server" Width="800px"></asp:TextBox>
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblActive" AssociatedControlID="chkActive" Text="Active:" />
                </td>
                <td>
                    <asp:CheckBox runat="server" ID="chkActive" Checked="false" /> (When unchecked, only visible on dev)
                </td>
            </tr>
            <tr>
                <td>
                    <asp:Label runat="server" ID="lblDay" AssociatedControlID="txtDay" Text="Active Date:" />
                </td>
                <td>
                    <asp:TextBox ID="txtDay" runat="server" CssClass="text" Columns="10" MaxLength="10" />
                </td>
            </tr>
            <asp:Literal runat="server" ID="litMoreWebSite" />
        </table>

        <asp:HiddenField ID="hidID" runat="server" />
        <asp:Button ID="btnSave" runat="server" Text="Save" OnClick="btnSave_Click" CssClass="submit" />
        <asp:Button ID="btnSaveAdd" runat="server" Text="Save & Add New" OnClick="btnSaveAdd_Click" CssClass="submit" /> &nbsp; &nbsp; &nbsp; 
        <a target="_blank" runat="server" id="lnkPreview" visible="false">See Preview</a>
    </div>
</asp:Content>
