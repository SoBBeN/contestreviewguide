﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SweepContestsThumbnail.aspx.cs" Inherits="SweepContestsThumbnail" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="//www.lovehopeluck.com/css/image.css" rel="stylesheet" />
    <script type="text/javascript" src="//www.themommyguide.com/js/jquery.Jcrop.min.js"></script>
    <link href="//www.lovehopeluck.com/css/jquery.Jcrop.css" rel="stylesheet" />
    <script type="text/javascript">
        function updateDivWidth(oriImgW, oriImgH, obj) {
            if (obj.id == '<%=imgCropbox.ClientID%>') {
                var imgW;

                if (oriImgW > 650) {
                    imgW = 650;
                }
                else {
                    var imgW = oriImgW;
                }

                document.getElementById("cropBoxContainer").style.width = imgW + 'px';
                document.getElementById("<%=imgCropbox.ClientID%>").onload = null;

                jQuery('.cropbox').Jcrop({
                    onSelect: storeCoords,
                    aspectRatio: 1
                });
            }
        }

        function storeCoords(c) {
            jQuery('#<%=X.ClientID%>').val(c.x);
            jQuery('#<%=Y.ClientID%>').val(c.y);
            jQuery('#<%=W.ClientID%>').val(c.w);
            jQuery('#<%=H.ClientID%>').val(c.h);
        };
    </script>
    <style type="text/css">
        #submain {
            width:650px;
            margin: 0px auto 0 auto;
        }

            #submain .image {
                text-align:center;
                margin: 10px auto 0 auto;
            }

            #submain .instructions {
                margin-top:8px;
            }

            #submain .buttons {
                text-align:center;
            }

                #submain .buttons .btnOK {
                    margin-top:8px;
                }
    </style>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolder1" Runat="Server">
    <div id="submain">
        <div class="image" id="cropBoxContainer">
            <img runat="server" id="imgCropbox" src="" class="cropbox" />
        </div>
        <div class="instructions">
            <img src="//www.themommyguide.com/images/MOMentsUploadInstructions.png" />
        </div>
        <br />
        <div class="buttons">
            <asp:ImageButton runat="server" ID="btnRotateL" ImageUrl="//www.lovehopeluck.com/images/MOMents_ImageEdit_RLeft.png" OnClick="btnRotate_ClickLeft" Text="Rotate Image Left" />
            <asp:ImageButton runat="server" ID="btnRotateR" ImageUrl="//www.lovehopeluck.com/images/MOMents_ImageEdit_Right.png"  OnClick="btnRotate_ClickRight" Text="Rotate Image Right" /><br />
            <asp:ImageButton runat="server" ID="btnCropImage" ImageUrl="//www.lovehopeluck.com/images/MOMents_ImageEdit_OK.png" OnClick="btnCrop_Click" Text="Crop Selection" CssClass="btnOK" />
        </div>
    </div>
    <br /><br />
    <asp:HiddenField ID="X" runat="server" />
    <asp:HiddenField ID="Y" runat="server" />
    <asp:HiddenField ID="W" runat="server" />
    <asp:HiddenField ID="H" runat="server" />
</asp:Content>
