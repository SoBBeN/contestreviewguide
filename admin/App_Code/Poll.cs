﻿using DB;
using System;
using System.Data;
using System.Text;

/// <summary>
/// Summary description for Poll
/// Source: http://www.clientsideasp.net/2009/03/01/aspnet-ajax-poll-using-jquery-a-complete-implementation-with-admin-part/
/// </summary>
public class Poll
{

    SqlServer db;

    /// <summary>
    /// ConString is the DB connection string specified in web.config
    /// </summary>
	public Poll()
	{
        db = new SqlServer();
	}


    /// <summary>
    /// Select poll by using ID
    /// </summary>
    /// <param name="pollID">ID of the poll to select.</param>
    public DataSet SelectPoll(int pollID)
    {
        return db.GetDataSet("usp_poll_select " + pollID.ToString());
    }

 /// <summary>
    /// Get a random active poll ID
    /// </summary>
    public int getRandomActivePollID()
    {
        object pollID = db.GetScalar("usp_Poll_getRandomActive");
        return pollID == null ? 0 : int.Parse(pollID.ToString());
    }


    /// <summary>
    /// list all the polls
    /// </summary>
    public DataTable ListPoll()
    {
        return db.GetDataTable("usp_poll_list");
    }

    /// <summary>
    /// Insert a poll and return the ID
    /// </summary>
    /// <param name="question"></param>
    /// <returns>PollID</returns>
    public int InsertPoll(string question, string blockMode, bool active)
    {
        StringBuilder query = new StringBuilder("[usp_poll_insert] ");

        query.Append(db.FormatSql(question)).Append(",");
        query.Append(db.FormatSql(blockMode)).Append(",");
        query.Append(db.FormatSql(active));

        object pollID = db.GetScalar(query.ToString());
        return pollID == null ? 0 : int.Parse(pollID.ToString());

    }

    /// <summary>
    /// Update the poll and return the number of rows affected
    /// </summary>
    /// <param name="pollID"></param>
    /// <param name="question"></param>
    /// <returns>Number of rows affected</returns>
    public int UpdatePoll(int pollID, string question, string blockMode, bool active)
    {
        StringBuilder query = new StringBuilder("[usp_poll_update] ");

        query.Append(db.FormatSql(pollID)).Append(",");
        query.Append(db.FormatSql(question)).Append(",");
        query.Append(db.FormatSql(blockMode)).Append(",");
        query.Append(db.FormatSql(active));

        return Convert.ToInt32(db.GetScalar(query.ToString()));
    }

    /// <summary>
    /// Delete a poll by using ID
    /// </summary>
    /// <param name="pollID"></param>
    /// <returns>Number of rows affected</returns>
    public int DeletePoll(int pollID)
    {
        return Convert.ToInt32(db.GetScalar("usp_poll_delete " + pollID.ToString()));
    }

    /// <summary>
    /// Insert choice of a poll
    /// </summary>
    /// <param name="pollID"></param>
    /// <param name="choice"></param>
    /// <returns>ID of the inserted choice</returns>
    public int InsertChoice(int pollID, string choice)
    {
        StringBuilder query = new StringBuilder("[usp_PollChoices_insert] ");

        query.Append(db.FormatSql(pollID)).Append(",");
        query.Append(db.FormatSql(choice));

        return Convert.ToInt32(db.GetScalar(query.ToString()));
    }

    /// <summary>
    /// Update a choice by its ID
    /// </summary>
    /// <param name="pollChoiceID"></param>
    /// <param name="choice"></param>
    /// <returns>Number of rows affected</returns>
    public int UpdateChoice(int pollChoiceID, string choice)
    {
        StringBuilder query = new StringBuilder("[usp_PollChoices_update] ");

        query.Append(db.FormatSql(pollChoiceID)).Append(",");
        query.Append(db.FormatSql(choice));

        return Convert.ToInt32(db.GetScalar(query.ToString()));
    }

    /// <summary>
    /// Delete multiple choices by IDs seperated by comma
    /// </summary>
    /// <param name="pollChoiceIDs">Comma seperated string of IDs</param>
    /// <returns>Number of rows affected</returns>
    public int DeleteChoice(int choiceID)
    {
        return Convert.ToInt32(db.GetScalar("usp_PollChoices_delete " + choiceID.ToString()));
    }

    /// <summary>
    /// Update the vote count for a choice by its ID
    /// </summary>
    /// <returns>Number of rows affected</returns>
    public int UpdateChoiceVote(int pollChoiceID)
    {
        return Convert.ToInt32(db.GetScalar("usp_PollChoices_vote " + pollChoiceID.ToString()));
    }

    /// <summary>
    /// Log the IP address of the user and the poll id
    /// </summary>
    /// <returns></returns>
    public int InsertPollIP(int pollID, string ip)
    {
        StringBuilder query = new StringBuilder("[usp_PollIPs_insert] ");

        query.Append(db.FormatSql(pollID)).Append(",");
        query.Append(db.FormatSql(ip));

        return Convert.ToInt32(db.GetScalar(query.ToString()));
    }

/// <summary>
    /// Select an entry by the IP address and the poll id
    /// </summary>
    /// <returns>Positive number if exists</returns>
    public int SelectPollIP(int pollID, string ip)
    {
        StringBuilder query = new StringBuilder("[usp_PollIPs_select] ");

        query.Append(db.FormatSql(pollID)).Append(",");
        query.Append(db.FormatSql(ip));

        object id = db.GetScalar(query.ToString());

        try { return id == null ? 0 : int.Parse(id.ToString()); }
        catch { return 0; }
    }

    public enum BlockMode
    {
        COOKIE = 1, IP_ADDRESS = 2, NONE = 3
    };

}
