﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;
using System.Web;

/// <summary>
/// Summary description for Functions
/// </summary>
public class Functions
{
    public static string StrToURL(string str)
    {
        return str.Replace("?", "").Replace("“", "").Replace("”", "").Trim().Replace(' ', '_').Replace('.', '_').Replace('…', '_').Replace(':', '_').Replace("&", "And").Replace("+", "And").Replace("/", "_And_").Replace("%", "_").Replace("\"", "");
	}

    public static string RemoveSpecialChars(string str)
    {
        return str.Replace("?", "").Replace("“", "").Replace('…', '_').Replace(':', '_').Replace("&", "And").Replace("#", "");
    }

    public static string ConvertToString(object obj)
    {
        if (DBNull.Value == obj)
            return string.Empty;
        else
            return Convert.ToString(obj);
    }

    public static string GetSha256(string str)
    {
        byte[] result;
        using (System.Security.Cryptography.SHA256 shaM = new System.Security.Cryptography.SHA256Managed())
        {
            result = shaM.ComputeHash(Encoding.Default.GetBytes(str));
        }
        return BitConverter.ToString(result).Replace("-", String.Empty);
    }

    public static string GetNonExistingFilename(string path, string filename)
    {
        int i = 0;
        string newfilename = filename;
        while (File.Exists(Path.Combine(path, newfilename)))
        {
            int index = filename.LastIndexOf(".");
            if (index >= 0)
                newfilename = filename.Substring(0, index) + (++i).ToString() + filename.Substring(index);
            else
                newfilename = filename.Substring(0, index) + (++i).ToString();
        }
        return newfilename;
    }
}