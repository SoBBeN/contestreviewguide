﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class ContentAdd : System.Web.UI.Page
{
    protected const string ITEMNAME = "Content";

    protected void Page_Load(object sender, EventArgs e)
    {
        divMsg.Visible = false;

        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null) //UPDATE MODE
            {
                hidID.Value = Request.QueryString["id"]; //Saving the ID to use later
                ShowExistingValues();
            }
        }
    }

    private void ShowExistingValues()
    {
        int id = int.Parse(hidID.Value);
        SqlDataReader dr = DB.DbFunctions.GetContent(id);

        if (dr.HasRows)
        {
            dr.Read();

            txtTitle.Text = Functions.ConvertToString(dr["Title"]);
            txtCategory.Text = Functions.ConvertToString(dr["Category"]);
            txtDescription.Text = Functions.ConvertToString(dr["DescriptionBtm"]);
            txtDescriptionTop.Text = Functions.ConvertToString(dr["DescriptionTop"]);
            chkActive.Checked = Convert.ToBoolean(dr["IsActive"]);
            lnkPreview.HRef = "http://omgsweeps-staging.azurewebsites.net/content/" + id + "/" + Functions.StrToURL(Functions.ConvertToString(dr["Category"])) + "/";
            imgImageFile.Src = System.Configuration.ConfigurationManager.AppSettings["baseimagescontentsurl"] + Functions.ConvertToString(dr["ImageFilename"]);
            txtImageLink.Text = Functions.ConvertToString(dr["ImageLink"]);
            lnkPreview.Visible = true;
            lnkRemoveImg.Visible = true;
            btnSave.Text = "Update " + ITEMNAME;

        }
        else //can't find the poll.. change to INSERT MODE
        {
            hidID.Value = String.Empty;
            btnSave.Text = "Save " + ITEMNAME;
        }
        dr.Close();
    }


    protected void btnSaveAdd_Click(object sender, EventArgs e)
    {
        btnSave_Click(sender, e);
        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int id = 0;

            string filename = String.Empty;
            if (ImageFile.HasFile)
            {
                filename = Functions.RemoveSpecialChars(ImageFile.FileName);

                var azureStorage = new AzureStorage("AzureStorageConnection");
                var blob = azureStorage.UploadBlog("couponinsanity", "images/content", ref filename, ImageFile.PostedFile.InputStream);
            }

            string authorImage = String.Empty;

            if (hidID.Value.Length == 0) //INSERT
            {
                id = DB.DbFunctions.InsertContent(txtTitle.Text, txtCategory.Text, txtDescriptionTop.Text, txtDescription.Text, chkActive.Checked, filename, txtImageLink.Text);

                if (id > 0)
                {
                    hidID.Value = id.ToString();
                    divMsg.InnerHtml = "Your new " + ITEMNAME + " has been created successfully.";
                    divMsg.Visible = true;
                }
            }
            else //UPDATE
            {
                id = int.Parse(hidID.Value);
                DB.DbFunctions.UpdateContent(id, txtTitle.Text, txtCategory.Text, txtDescriptionTop.Text, txtDescription.Text, chkActive.Checked, filename, txtImageLink.Text);

                divMsg.InnerHtml = "Your " + ITEMNAME + " has been updated successfully.";
                divMsg.Visible = true;
            }

            if (id > 0)
            {
                ShowExistingValues();
            }
        }
    }
    protected void lnkRemoveImg_Click(object sender, EventArgs e)
    {
        int id = int.Parse(hidID.Value);
        DB.DbFunctions.DeleteContentImage(id);
        ShowExistingValues();
    }
}