﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class SettlementsRebates : System.Web.UI.Page
{
    protected const string ITEMNAME = "Settlements & Rebates";

    protected void Page_PreRender(object sender, EventArgs e)
    {
        divMsg.Visible = false;
        if (!IsPostBack)
        {
            FillGV();
        }
    }

    private void FillGV()
    {
        SqlDataReader dt = DB.DbFunctions.GetAllSettlementsRebates();

        gv.DataSource = dt;
        gv.DataBind();
    }

    protected void GvRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gv.DataKeys[e.RowIndex]["ID"].ToString());
        DB.DbFunctions.DeleteSettlementsRebates(id);

        divMsg.InnerHtml = "Selected " + ITEMNAME + " deleted successfully.";
        divMsg.Visible = true;

        FillGV();
    }
}