﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.IO;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class LifestyleAdd : System.Web.UI.Page
{
    protected const string ITEMNAME = "Luck Letter Article";

    protected void Page_Load(object sender, EventArgs e)
    {
        divMsg.Visible = false;

        if (!IsPostBack)
        {
            if (Request.QueryString["id"] != null) //UPDATE MODE
            {
                hidID.Value = Request.QueryString["id"]; //Saving the ID to use later
                ShowExistingValues();
            }
            int recipeid;
            if (!int.TryParse(hidID.Value, out recipeid))
                recipeid = int.MinValue;

            FillDdl(DB.DbFunctions.GetLifestyleCategory(recipeid));
            FillDdl(DB.DbFunctions.GetLifestyleTag(recipeid), sltTag);
        }
    }

    private void FillDdl(SqlDataReader dr, MyClassLibrary.Controls.Selectable slt)
    {
        if (dr != null)
        {
            if (dr.HasRows)
            {
                slt.Items.Clear();
                while (dr.Read())
                {
                    ListItem i = new ListItem(Convert.ToString(dr["Description"]), Convert.ToString(dr["ID"]));
                    i.Selected = Convert.ToBoolean(dr["IsSelected"]);
                    slt.Items.Add(i);
                }
            }
            dr.Close();
        }
    }

    private void FillGV(int lifestyleid)
    {
        SqlDataReader dt = DB.DbFunctions.GetAllLifestyleSlide(lifestyleid);

        gv.DataSource = dt;
        gv.DataBind();
    }

    private void FillDdl(SqlDataReader dr)
    {
        if (dr != null)
        {
            if (dr.HasRows)
            {
                while (dr.Read())
                {
                    ListItem i = new ListItem(Convert.ToString(dr["Description"]), Convert.ToString(dr["ID"]));
                    i.Selected = Convert.ToBoolean(dr["IsSelected"]);
                    switch (Convert.ToInt32(dr["WebSiteID"]))
                    {
                        case 1:
                            sltCategoryTmg.Items.Add(i);
                            fsTMG.Visible = true;
                            break;
                        case 2:
                            sltCategoryCI.Items.Add(i);
                            fsCI.Visible = true;
                            break;
                        case 12:
                            sltCategoryC2W.Items.Add(i);
                            fsC2W.Visible = true;
                            break;
                        case 13:
                            sltCategoryFMG.Items.Add(i);
                            fsFMG.Visible = true;
                            break;
                        case 16:
                            sltCategoryOMG.Items.Add(i);
                            fsOMG.Visible = true;
                            break;
                        case 17:
                            sltCategoryC4R.Items.Add(i);
                            fsC4R.Visible = true;
                            break;
                        case 18:
                            sltCategoryACG.Items.Add(i);
                            fsACG.Visible = true;
                            break;

                        case 6:
                            sltCategoryTpg.Items.Add(i);
                            fsPG.Visible = true;
                            break;
                        case 29:
                            sltCategoryTAG.Items.Add(i);
                            fsTAG.Visible = true;
                            break;
                        case 37:
                            sltCategoryHMG.Items.Add(i);
                            fsHMG.Visible = true;
                            break;
                        case 47:
                            sltCategoryFSG.Items.Add(i);
                            fsFSG.Visible = true;
                            break;
                        case 59:
                            sltCategoryTSG.Items.Add(i);
                            fsTSG.Visible = true;
                            break;
                        case 50:
                            sltCategoryPFG.Items.Add(i);
                            fsPFG.Visible = true;
                            break;
                        case 53:
                            sltCategoryTWM.Items.Add(i);
                            fsTWM.Visible = true;
                            break;
                        case 36:
                            sltCategoryCAG.Items.Add(i);
                            fsCAG.Visible = true;
                            break;
                        case 63:
                            sltCategoryCRG.Items.Add(i);
                            fsCRG.Visible = true;
                            break;
                        case 64:
                            sltCategoryFIS.Items.Add(i);
                            fsFIS.Visible = true;
                            break;
                    }
                }
            }
            dr.Close();
        }
    }

    private void ShowExistingValues()
    {
        int id = int.Parse(hidID.Value);
        SqlDataReader dr = DB.DbFunctions.GetLifestyle(id);

        if (dr.HasRows)
        {
            dr.Read();

            txtTitle.Text = Functions.ConvertToString(dr["Title"]);
            txtSubtitle.Text = Functions.ConvertToString(dr["SubTitle"]);
            txtDay.Text = Functions.ConvertToString(dr["ActiveDate"]);
            chkActive.Checked = Convert.ToBoolean(dr["IsActive"]);
            lnkPreview.HRef = System.Configuration.ConfigurationManager.AppSettings["basepreviewurl"]+"LifeStyle/" + id + "/" + Functions.StrToURL(Functions.ConvertToString(dr["Category"])) + "/" + Functions.StrToURL(Functions.ConvertToString(dr["Title"])) + "/";
            lnkPreview.Visible = true;
            btnSave.Text = "Update " + ITEMNAME;
            lblActive.Text = "Active on " + System.Configuration.ConfigurationManager.AppSettings["Title"] + ": ";
            FillGV(id);
            lnkNewSlide.HRef = "LifestyleSlideAdd.aspx?fid=" + id.ToString();
            lnkNewSlide.InnerText = "Add New Slide";
            lnkNewSlide.Disabled = false;
        }
        else //can't find the poll.. change to INSERT MODE
        {
            hidID.Value = String.Empty;
            btnSave.Text = "Save " + ITEMNAME;
        }
        dr.Close();
    }


    protected void btnSaveAdd_Click(object sender, EventArgs e)
    {
        btnSave_Click(sender, e);
        Response.Redirect(Request.Url.GetLeftPart(UriPartial.Path));
    }

    protected void btnSave_Click(object sender, EventArgs e)
    {
        if (Page.IsValid)
        {
            int id = 0;

            string activeDate;
            if (txtDay.Text.Length > 7)
                activeDate = txtDay.Text;
            else
                activeDate = null;

            if (hidID.Value.Length == 0) //INSERT
            {
                id = DB.DbFunctions.InsertLifestyle(txtTitle.Text, chkActive.Checked, activeDate, txtSubtitle.Text);

                //POLL INSERTED SUCCESSFULLY
                if (id > 0)
                {
                    hidID.Value = id.ToString();
                    divMsg.InnerHtml = "Your new " + ITEMNAME + " has been created successfully.";
                    divMsg.Visible = true;
                }
            }
            else //UPDATE
            {
                id = int.Parse(hidID.Value);
                DB.DbFunctions.UpdateLifestyle(id, txtTitle.Text, chkActive.Checked, activeDate, txtSubtitle.Text);

                divMsg.InnerHtml = "Your " + ITEMNAME + " has been updated successfully.";
                divMsg.Visible = true;
            }

            if (id > 0)
            {
                foreach (ListItem item in sltCategoryTmg.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryTpg.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryCI.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryC4R.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryTAG.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryHMG.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryFSG.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryACG.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryOMG.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryC2W.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryTSG.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryPFG.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryCAG.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryTWM.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryFMG.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryCRG.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                foreach (ListItem item in sltCategoryFIS.Items)
                {
                    if (item.Selected)
                        DB.DbFunctions.InsertLifestyleCategory(id, Convert.ToInt32(item.Value));
                }
                ShowExistingValues();
            }
        }
    }



    protected void GvRowDeleting(object sender, GridViewDeleteEventArgs e)
    {
        int id = int.Parse(gv.DataKeys[e.RowIndex]["ID"].ToString());

        DB.DbFunctions.DeleteLifestyleSlide(id);

        divMsg.InnerHtml = "Selected " + ITEMNAME + " deleted successfully.";
        divMsg.Visible = true;

        FillGV(Convert.ToInt32(hidID.Value));
    }

    protected void GvRowCommand(Object sender, GridViewCommandEventArgs e)
    {
        GridViewRow gvr = (GridViewRow)(((LinkButton)e.CommandSource).NamingContainer);
        int RowIndex = gvr.RowIndex;
        int id = int.Parse(gv.DataKeys[RowIndex]["ID"].ToString());
        bool moveup = (e.CommandName == "MoveUp");

        DB.DbFunctions.MoveLifestyleSlide(id, moveup);
        FillGV(Convert.ToInt32(hidID.Value));

    }
}