﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class DailyNumberSign : System.Web.UI.Page
{
    protected void Page_Load(object sender, EventArgs e)
    {
        litTitle.Text = Page.RouteData.Values["sign"].ToString() + " Daily Lucky Numbers";
        imgSign.Src = System.Configuration.ConfigurationManager.AppSettings["AssetPath"] + "/images/" + Page.RouteData.Values["sign"].ToString().ToLower() + "_sign.png";
        
        int min = 1;
        int max = 99;
        Random r=new Random();
        SortedList<int, int> perm=new SortedList<int, int>();
        for (int i=min; i<=max; i++) perm.Add(r.Next(), i);
    }
}