﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="SweepContests.aspx.cs" Inherits="SweepContests" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>
<%@ Register TagPrefix="fbc" TagName="FbComments" Src="~/UserControls/FbComments.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="/css/pointsoffers.css" rel="stylesheet" />
    <script type="text/javascript">
        $(document).ready(function () {
            if (matchMedia('only screen and (min-width: 310px) and (max-width:500px)').matches) {
                //$('.ads_padding_middle').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- Free Samples - Bottom --><ins class="adsbygoogle" style="display:block;width:98%;height:90px" data-ad-client="ca-pub-0634471641041185" data-ad-slot="7335364784"></ins><script>        (adsbygoogle = window.adsbygoogle || []).push({});<\/script>');
            }
        });
    </script>
</asp:Content>

<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />
    <div id="topcoupon">
        <div class="title"><asp:Literal runat="server" ID="litTitle" /></div>
        <div class="date"><asp:Literal runat="server" ID="litDate" /></div>
        <div class="tags">Categories: <asp:Literal runat="server" ID="litTags" /></div>
        <div class="image">
            <img runat="server" id="imgTopCoupon" />
        </div>
        <div class="text"><asp:Literal runat="server" ID="litText" /></div>
        <div class="button">
            <a href="" runat="server" id="lnkBtn" target="_blank">Enter</a>
        </div>
        <div id="socialst">
            <span class='st_facebook_hcount item' displayText='Facebook'></span>
            <span class='st_twitter_hcount item' displayText='Tweet'></span>
            <span class='st_pinterest_hcount item' displayText='Pinterest'></span>
            <span class='st_email_hcount item' displayText='Email'></span>
        </div>
    </div>
    <br />
    <div id="poPrevNext">
        <div class="prev image"><a href="javascript:void(0);" runat="server" id="imgLnkPrev"><img runat="server" id="imgPrev" /></a></div>
        <div class="prev lnk"><a href="javascript:void(0);" runat="server" id="lnkPrev">< previous</a></div>
        <div class="next image"><a href="javascript:void(0);" runat="server" id="imgLnkNext"><img runat="server" id="imgNext" /></a></div>
        <div class="next lnk"><a href="javascript:void(0);" runat="server" id="lnkNext">next ></a></div>
        <div style="clear:both;"></div>
    </div>
    <div style="clear:both;"></div>
    <br /><br />
    <div id="fbCommentsPlaceholder">
        <fbc:FbComments id="fbComments1" runat="server" />
    </div>
    <br /><br />
</asp:Content>
