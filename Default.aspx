﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Default.aspx.cs" Inherits="_Default" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="/css/news.css" rel="stylesheet" />
    <link href="/css/lifestyle.css" rel="stylesheet" />
    <link href="/css/jobs.css" rel="stylesheet" />

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <style>
        .homeSections .ad6 a img {
            margin:0 auto !important;
        }
    </style>
    <div id="content">
        <div class="ckedit"><asp:Literal runat="server" ID="litDescriptionTop"></asp:Literal></div>
        <asp:Literal runat="server" ID="litImage"></asp:Literal>
        <div class="ckedit"><asp:Literal runat="server" ID="litDescriptionBtm"></asp:Literal></div>
<%--        <h1 class="clear"><a href="/Lifestyle"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/icons_local_job.png" /><asp:Literal runat="server" ID="litLifestylesTitle">Latest In <span>Contest Talk</span></asp:Literal></a></h1>
        <hr class="blueline" />
        <div id="lifestyles">
            <asp:Literal runat="server" ID="litArticles"></asp:Literal>
        </div>--%>
                
       <br class="clear" />
        <h1><a href="/News"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/icons_news.png" /><asp:Literal runat="server" ID="litTitleAstroTalk">Latest <span>In The News</span></asp:Literal></a></h1>
        <hr class="blueline" />
        <div id="news">
            <asp:Literal runat="server" ID="litNews"></asp:Literal>
        </div>

        <div class="clear"></div>
                        <hr />
        <div class="homeSections">
            <div><a href="/Contests"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/4_hottestsweeps.png" /></a></div>
            <div><a href="/Content/152/Sweepstakes_Tips_And_Tricks/"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/contesttips.png" /></a></div>
            <div><a href="/DailyNumbers/"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/4_dailyluckynumbers.png" /></a></div>
            <div><a href="/Samples/"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/4_getfeesamples.png" /></a></div>
        </div>
<%--        <div class="ads_padding_top ads_padding_middle"></div>--%>
    </div>
    <div class="clear"></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="OutsideForm" Runat="Server">
<%--        <script type="text/javascript">
            $(function () {
                dataLayer.push({ event: 'hmgAnalytics' });
            });
    </script>--%>
</asp:Content>




