﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/MasterPage2.master" CodeFile="RecipeCategory.aspx.cs" Inherits="RecipeCategory" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>
<%@ Register TagPrefix="fbc" TagName="FbComments" Src="~/UserControls/FbComments.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="ContentPlaceHolderHead" Runat="Server">
    <link href="/css/recipe.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderLeft" Runat="Server">
    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />
    <div class="header">
        <img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/recipeinsanity.png" />
    </div>
    <div id="recipe">
        <div class="main">
            <div class="left">
                <div class="categories"><asp:Literal runat="server" ID="litCategories"></asp:Literal></div>
            </div>
            <div class="right">
                <div class="title"><asp:Literal runat="server" ID="litTitle"></asp:Literal></div>
                <div class="OtherRecipes"><asp:Literal runat="server" ID="litOtherRecipes"></asp:Literal></div>
            </div>
            <div style="clear:both;"></div>
            <br /><br />
        </div>
        <div id="poPrevNext">
            <div class="prev"><a runat="server" id="lnkPrev" class="lnk" visible="false">< previous</a></div>
            <div class="next"><a runat="server" id="lnkNext" class="lnk" visible="false">next ></a></div>
            <div style="clear:both;"></div>
        </div>
        <div style="clear:both;"></div>
        <br /><br />
    </div>
</asp:Content>
