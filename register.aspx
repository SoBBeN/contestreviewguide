﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPageRegister.master" AutoEventWireup="true" CodeFile="register.aspx.cs" Inherits="register" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
        
        <!-- Custom Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,700,800" rel="stylesheet" />
      	<link href="https://fonts.googleapis.com/css?family=Kanit:300,400,500,600" rel="stylesheet" />


      
      	<!-- Font Awesome -->
      	<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.1.0/css/all.css" integrity="sha384-lKuwvrZot6UHsBSfcMvOkWwlCMgc0TaWr+30HWe3a4ltaBwTZhyTEggF5tJv8tbt" crossorigin="anonymous">
            <!-- Bootstrap CSS -->
        <link href="/css/bootstrap.min.css" rel="stylesheet" />
            <!-- Animate.css -->
        <link href="/css/animate.css" rel="stylesheet" />
        <!-- Custom CSS -->
       	<link href="/css/CRG_uniqueLP.css?rev=026" rel="stylesheet" />
      	<link href="/css/base.css" rel="stylesheet" />

    	<!-- jQuery -->           
   <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/jquery-ui.min.js"></script>
      	<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.11.1/themes/smoothness/jquery-ui.css" />

        <script type="text/javascript" src="/js/jquery.mask.min.js"></script>
        <script src="/js/validator.js"></script>
    <script type="text/javascript" src="/js/moment.js"></script>
  
      
                       <!-- Bootstrap JS -->
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
      
      <script type="text/javascript">
          function buildDOBDropdown(dobFull, dobYear, dobMonth, dobDay)
          {
            var currentYear = new Date().getFullYear();
            var startYear = currentYear - 18;
            var endYear = startYear - 82;

            // Set up first Year option
            $('#ddl_dob_year').append($('<option>',
                    {
                        value: "",
                        text: "Birth"
                    })
                );

            for (var i = startYear; i > endYear; i--) {
                $('#ddl_dob_year').append($('<option>',
                    {
                        value: i,
                        text: i
                    })
                );
            }

            if (dobFull != '') { // individual dob values are preset to current date in the back-end, so we check if full dob is set before we preselect
                if (dobYear)
                    $("#ddl_dob_year option[value='" + dobYear + "']").prop('selected', 'selected');
                if (dobMonth)
                    $("#ddl_dob_month option[value='" + this.padWithZeros(dobMonth, 2) + "']").prop('selected', 'selected');
                if (dobDay)
                    $("#ddl_dob_day option[value='" + this.padWithZeros(dobDay, 2) + "']").prop('selected', 'selected');
            }
        }

          function getAge(dobValue)
          {
            if (dobValue != '')
                return moment().diff(dobValue, 'years');
            else
                return 17; // default age to 17 so criteria doesn't wrongly get triggered
          }

        function prepFormFieldError() {
            $('input').on('blur', function(){
                $(this).addClass('onX');
                $(this).next('div').addClass('onX');
             });
             $('button').on("click",function() {
                $('input').addClass('onX');
                $('input').next('div').addClass('onX');
              });
        }

	   function CheckBoxIsChecked(Checkbox) {
	       if ($(Checkbox).is(':checked')) {return true;}
	       else{return false;}
	   }

        function validateFields(arrayOfFieldsID,submitFormOnSuccess,popUpURL,zipLookupAccessToken)
        {
                var NameFieldRegex = /^[a-zA-Z.\\'\\-\\s]+$/;
                var ZipFieldRegex = /^\d{5}(?:[-]\d{4})?$/;
                var PhoneFieldRegex = /^\d{10}$/;
                var emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
                var isInputValid = true;
            var isSlideValid = true;
            var alertListError = [];

                
                  for (i = 0; i < arrayOfFieldsID.length; i++) {
                      switch (arrayOfFieldsID[i]) {

                        case '#disclaimer_1': //disclaimer1
                            isInputValid = CheckBoxIsChecked('#disclaimer_1');
                            if (isInputValid == false) { isSlideValid = false;alertListError.push("Please agree to the Terms and Conditions and Privacy Policy to continue.");}                           
                        break;
                        case '#txt_email': //First Name
                        isInputValid = emailRegex.test($(arrayOfFieldsID[i]).val());
                        if(isInputValid == false){isSlideValid = false}
                            break;
                        case '#txt_first_name': //First Name
                        isInputValid = NameFieldRegex.test($(arrayOfFieldsID[i]).val());
                        if(isInputValid == false){isSlideValid = false}
                            break;
                        case '#txt_last_name': //Last Name
                         isInputValid = NameFieldRegex.test($(arrayOfFieldsID[i]).val());
                          if(isInputValid == false){isSlideValid = false}
                            break;
                        case '#txt_zip': // Zip
                        isInputValid = ZipFieldRegex.test($(arrayOfFieldsID[i]).val());
                          if(isInputValid == false){isSlideValid = false}else{}
                            break;
                        case '#txt_address': //Address
                            if ($(arrayOfFieldsID[i]).val().trim() == "") {
                                isSlideValid = false;
                            }
                            break;
                        case '#txt_phone': // phone
                            $('#txt_phone').removeAttr("pattern");
                            $('#txt_phone').unmask();
                            isInputValid = PhoneFieldRegex.test($(arrayOfFieldsID[i]).val());
                            if (isInputValid == false) {
                              isSlideValid = false;
                            }
                            break;
                        case '#hdn_dob': //dob 
                        
                                if ($('#ddl_dob_month').length && $('#ddl_dob_day').length && $('#ddl_dob_year').length)
                                {
                                    $('#hdn_dob').val($('#ddl_dob_month').val() + "/" + $('#ddl_dob_day').val() + '/' + $('#ddl_dob_year').val());
                                }
                               var age = getAge($('#hdn_dob').val());
                               if(age >= 18)
                               { 
                                    if($('#hdn_dob').val().length == 10) // dob should always be mm/dd/yyyy format
                                    {
                                        var dateOfBirth = moment($('#hdn_dob').val(), 'MM/DD/YYYY');
                                        isInputValid = (dateOfBirth != null && dateOfBirth.isValid());
                                        if(isInputValid == false){
                                          isSlideValid = false;
                                        } 
                                    }
                                    else
                                    {

                                        isSlideValid = false;
                                    }
                                }
                                else
                               {
                                   $('#ddl_dob_month').click();
                                    isSlideValid = false;

                                }
                            break;

                    
                    }//end switch
                }//end for
            if (isSlideValid == false) {
                    var errorDisplay = "";
                   $('#txt_phone').prop("pattern",  "^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}$");
                   $('#hdn_dob').mask('00/00/0000');
                    $('#txt_phone').mask('(000) 000-0000');
                    for (i = 0; i < alertListError.length; i++) {
                        errorDisplay = errorDisplay + alertListError[i] + " \n";
                    }
                    if (errorDisplay != "") {
                        alert(errorDisplay);
                    }

                }
                else
                {
                  if(submitFormOnSuccess == true)
                  {
                    $('.submitForm').prop('disabled', true);

                    if(popUpURL != "" && popUpURL != "undefined")
                    {
                      window.open(popUpURL, '_blank');
                    }
                        var email = $('#remotePenCheckEmail').val();
                        if(email == "undefined" || email == null){email = "";}
                        if(email.indexOf("@gmail") == -1)
                        {
                            if (getAge($('#hdn_dob').val()) >= 40) 
                            {
                               $('#remotePenCriteriaPassed').val('true');
                            }
                        }
                    //if zip exist make sure to give time for lookup is done else submit form
                    if($('#txt_zip').val())
                    {
                        if($('#zip_lookup_done').val() == "true")
                        {
                            $('form').submit();
                        }
                        else
                        {
                            setTimeout(function(){ $('form').submit(); }, 1500);
                        }
                    }
                    else
                    {
                         $('form').submit();
                    }
           

                    
                     
                  }
                 
                }
                 return isSlideValid;
    
          }
          function prepFormContact(state) {
              prepFormFieldError();
                          $('#txt_first_name').prop("pattern",  "^[a-zA-Z ]*$" );
            $('#txt_last_name').prop("pattern",  "^[a-zA-Z ]*$");
            $('#txt_zip').prop("pattern",  "^\\d{5}(?:[-\\s]\\d{4})?$");
                          $('#txt_city').prop("pattern",  "^[a-zA-Z.\\\\'\\\\-\\\\ ]+$" );
            $('#txt_phone').prop("pattern",  "^(\\+\\d{1,2}\\s)?\\(?\\d{3}\\)?[\\s.-]\\d{3}[\\s.-]\\d{4}$");
            //$('#txt_dob').prop("pattern",  "((0[13578]|1[02])[\\/.]31[\\/.](18|19|20)[0-9]{2})|((01|0[3-9]|1[1-2])[\\/.](29|30)[\\/.](18|19|20)[0-9]{2})|((0[1-9]|1[0-2])[\\/.](0[1-9]|1[0-9]|2[0-8])[\\/.](18|19|20)[0-9]{2})|((02)[\\/.]29[\\/.](((18|19|20)(04|08|[2468][048]|[13579][26]))|2000))");
            //$('#hdn_dob').prop("pattern",  "((0[13578]|1[02])[\\/.]31[\\/.](18|19|20)[0-9]{2})|((01|0[3-9]|1[1-2])[\\/.](29|30)[\\/.](18|19|20)[0-9]{2})|((0[1-9]|1[0-2])[\\/.](0[1-9]|1[0-9]|2[0-8])[\\/.](18|19|20)[0-9]{2})|((02)[\\/.]29[\\/.](((18|19|20)(04|08|[2468][048]|[13579][26]))|2000))");
            $('#txt_dob').prop("pattern",  "((1[0-2])|(0[1-9]))\\/((0[1-9])|((1|2)[0-9])|(3[0-1]))\\/((19|20)\\d{2})");
            $('#hdn_dob').prop("pattern",  "((1[0-2])|(0[1-9]))\\/((0[1-9])|((1|2)[0-9])|(3[0-1]))\\/((19|20)\\d{2})");
            
 			$('#hdn_dob').mask('00/00/0000');
            $('#txt_phone').mask('(000) 000-0000');
            $('#txt_dob').mask('00/00/0000');
        
          }
  
      </script>
    <script>
        $(document).ready(function () {
           $('#ddl_dob_month, #ddl_dob_day, #ddl_dob_year').click(function () {
               var dob_m = $('#ddl_dob_month').val();
               var dob_d = $('#ddl_dob_day').val();
               var dob_y = $('#ddl_dob_year').val();
               if (dob_m == "" || dob_d == "" || dob_y == "")
               {
                   $('.dob_error').show();
               }
               else
               {
                   $('.dob_error').hide();
               }
          });    
        });
    </script>
        <style>         
          
          input:invalid.onX {
              border-color:#a94442;
          }
          input:valid + .help-block { 
              display: none;
          }
          input:invalid + .help-block { 
              display: none;
          }
          input:invalid + .help-block.onX { 
              display: block;
              color:#a94442;
              font-size: 12px;
          }
   .form-control {
      padding: 1rem;
      box-shadow: 0px 0px 4px black;
      width: 95%;
      margin: auto;
      margin-top: 16px;
   }       
          
    .subcopy {
      text-shadow: 0px 3px 6px #000000;
      padding-bottom:0;
      font-size: 44px;
      color:#F6CC27; 
      font-family: 'kanit','Open Sans', sans-serif;
      font-weight:600;
      text-align: center;
      line-height: 1.1;
          margin-bottom: 0;
    }
     
    #subtitle {
      font-size: 40px;
      font-weight: 500;
      margin-top: 0px;
      margin-bottom: 20px; 
      text-align: center; 
      color: #ffffff;
      font-family: 'kanit','Open Sans', sans-serif;
    }
img#logo {
    width: 50%;
    padding-top: 2%;
}
  footer.footer {
    margin-top: 10%;
    color: #ffffff;
    font-family: 'Kanit','Open Sans', sans-serif;
    font-weight: 300;
}
  footer .legal{
  	width: 95%;
    margin: 0 auto 10px;
    font-weight: 300;
    line-height: 1.1;
    font-size: 14px;
  }
    footer.footer a {
      font-family: 'Kanit','Open Sans', sans-serif;
    font-weight: 300;
    color: #ffffff;
}
                 
/*question css start*/
   .PFG_SingleLayout_1203 .sweeps{
    display:none;
   }     
   .PFG_SingleLayout_1203 .form_bg {
    background: rgba(255, 255, 255, 0.6);
    padding: 3%;
    width:95%;
    max-width:700px;
    margin-top: 2%;
    margin-bottom: 2%;
    display: inline-block;
    border-radius: 15px;
}   
.PFG_SingleLayout_1203 .coreg_buttons > a {
  color:white;
  background-color: #49882C; 
  text-decoration:none;
  font-family: 'kanit','Open Sans', sans-serif;
  width: calc(50% - 30px);
  padding: 15px;
  transition-duration: 0.4s;
}
.PFG_SingleLayout_1203 .coreg_buttons > a:hover {
          box-shadow: 0px 3px 20px #1c1c1c;
}
.PFG_SingleLayout_1203 .coreg_question {
	font-family: 'kanit','Open Sans', sans-serif;
    font-weight: 600;
    font-size: 25px;
}
.PFG_SingleLayout_1203 .draw_footer_container {
  	font-family: 'kanit','Open Sans', sans-serif;
    color: #ffffff;
    margin-top: 12%;
    color: black;
}
.dob_error {
            display: none;
    color: #a94442;
    font-size: 12px;
            }
          
/*question css end*/
@media(max-width:768px){
  .subcopy {
    font-size: 35px;
  }
  #subtitle {
    font-size: 30px;
  } 
  .PFG_SingleLayout_1203 .coreg_buttons > a {
    width: calc(50% - 10px);
	}
    .PFG_SingleLayout_1203 .coreg_buttons > a {
      margin: 10px 5px;
	}
}
@media (max-width:500px) { 
    	footer.footer {
    margin-top: 5%;
}
    footer .legal {
    font-size: 12px;
}
    footer.footer a {
     font-size: 12px;
	}
   .subcopy {
     font-size: 25px;
   }
  #subtitle {
    font-size: 22px;
  }
} 
          </style>
      
      
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">

     <header>
          
         <div class="container">
      <div class="logo text-center">
                  <img src="/images/contestreview_logo.png" id="logo" />
          </div>
       </div>
       <div class="container" id="head">
              <h1 class="questions subcopy" id="head">SIGN UP FOR DAILY CONTEST REVIEWS!</h1>
    <!--  <h2 id="subtitle">Get approved within 24 hours!</h2> -->
         <br />
        </div>
    
          
        </header>
    <style>
    #skipButton{
   display:none;
  }
   footer .legal {
  	display:none;
  }
   .form_container
  {
    background: rgba(255, 255, 255, 0.6);
    margin-right: auto;
    margin-left: auto;
    margin-top: 0%;
    width: 95%;
    max-width: 460px;
    border-radius: 20px;
    padding:20px 10px;

  }
    #consent{
        padding-right:20px;
    }
    
    .disclaimertext{
    text-align: center;
    font-size: 9pt;
    padding-top: 10px;
    }
   .form-control {
    padding: 1rem;
    box-shadow: 0px 0px 4px black;
    width: 95%;
    max-width: 360px;
    margin: auto;
    margin-top: 16px; 
}
     #txt_email {
    margin-top: 0px; 
}
   Select {
padding: 1rem;
    box-shadow: 0px 0px 4px black;
    margin: auto;
    margin-top: 16px;
    width: calc(33% - 3px);
  }

   .selectWrapper{
      width: 95%;
    max-width: 360px;
    margin: auto;
  }
 input#btnSubmit {
    background-color: #b22a20;
    color: #ffffff;
    font-family: 'Open Sans', sans-serif;
    font-weight: 700;
    font-size: 36px;
    border-color: #1075c1;
    text-align: center;
    display: block;
    max-width: 360px;
    width: 95%;
    margin: 10px auto;
}
      #disclaimer_1 {
        float: left;
        margin-top: 5%;
     }
  @media(max-width:500px){
  	 input#btnSubmit {
    	font-size: 27px;
	}
     .form-control {
    	padding: .75rem;
	}
     Select {
    	padding: .75rem;
    }
  }
    @media(max-width:375px){
  	 input#btnSubmit {
    	font-size: 24px;
	}
  }
</style>

 
<div class="container">
<!-- multistep form -->
<div class="form_container uniform_margin">
<form id="msform" method="post" action="/thankyou.aspx">
    <div class="inner">
                            <div class="input_wrapper_outer">
                                <div class="input_wrapper">
  <!--Needed-->
  <input type="hidden" name="remotePenCriteriaPassed" id="remotePenCriteriaPassed" value="false" />
  <input type="hidden" name="zip_lookup_done" id="zip_lookup_done" value="" />
  <input type="hidden" name="remotePenCheckEmail" id="remotePenCheckEmail" value="" />

  <!-- fieldsets -->
  <fieldset>
   
      <div class="inner_input">
    <input type="text" name="email" id="txt_email" value="" placeholder="Email" class="form-control" autofocus="autofocus" required />
    <div class="help-block with-errors text-center">Please enter your email</div>
    </div>
      
    <div class="inner_input">
    <input type="text" name="first_name" id="txt_first_name" value="" placeholder="First Name" class="form-control" required />
    <div class="help-block with-errors text-center">Please enter your first name</div>
    </div>
      
   	<div class="inner_input">
    <input type="text" name="last_name" id="txt_last_name" value="" placeholder="Last Name" class="focus form-control" required/>
      <div class="help-block with-errors text-center">Please enter your last name</div> 
    </div>
    <div class="inner_input">
    <input type="tel" name="zip" id="txt_zip" value="" placeholder="Zip Code" class="focus form-control" required />
    <div class="help-block with-errors text-center">Please enter a valid zip code</div>
    </div>
    <div class="inner_input">
    	<input type="text" name="address_1" id="txt_address" value="" placeholder="Address" class="focus form-control" required />
    	<div class="help-block with-errors text-center">Please enter a valid address</div>
    </div>
    <div class="inner_input">
      <div class="selectWrapper">
      <select name="dob_m" id="ddl_dob_month" class="slide_dob">
        <option selected="selected" value="">Date</option>
        <option value="01">Jan</option>
        <option value="02">Feb</option>
        <option value="03">Mar</option>
        <option value="04">Apr</option>
        <option value="05">May</option>
        <option value="06">Jun</option>
        <option value="07">Jul</option>
        <option value="08">Aug</option>
        <option value="09">Sep</option>
        <option value="10">Oct</option>
        <option value="11">Nov</option>
        <option value="12">Dec</option>
      </select>

      <select name="dob_d" id="ddl_dob_day" class="slide_dob">
        <option selected="selected" value="">Of</option>
        <option value="01">01</option>
        <option value="02">02</option>
        <option value="03">03</option>
        <option value="04">04</option>
        <option value="05">05</option>
        <option value="06">06</option>
        <option value="07">07</option>
        <option value="08">08</option>
        <option value="09">09</option>
        <option value="10">10</option>
        <option value="11">11</option>
        <option value="12">12</option>
        <option value="13">13</option>
        <option value="14">14</option>
        <option value="15">15</option>
        <option value="16">16</option>
        <option value="17">17</option>
        <option value="18">18</option>
        <option value="19">19</option>
        <option value="20">20</option>
        <option value="21">21</option>
        <option value="22">22</option>
        <option value="23">23</option>
        <option value="24">24</option>
        <option value="25">25</option>
        <option value="26">26</option>
        <option value="27">27</option>
        <option value="28">28</option> 
        <option value="29">29</option>
        <option value="30">30</option>
        <option value="31">31</option>
      </select>
      <select name="dob_y" id="ddl_dob_year" class="slide_dob"></select>
        </div>
      </div>
       <div class="inner_input">
      <input type="tel" name="dob" class="focus" style="display:none;" id="hdn_dob" value="" placeholder="(mm/dd/yyyy)" required />
      <div class="help-block with-errors dob_error text-center">Please enter a valid birthday</div>  
      
    </div>
    <div class="inner_input">
    	<input type="tel" name="phone" id="txt_phone" value="" placeholder="(123) 456-7890" class="focus form-control" required/>
    	<div class="help-block with-errors text-center">Please enter a valid phone number</div>
    </div>  
        <div class="row">
  	<div class="col-lg-12">
          <input type="checkbox" name="opt_in" id="disclaimer_1" value="true" />
            <asp:Literal runat="server" ID="litDisclaimer1"></asp:Literal>
                                      <br />
                                      <asp:Literal runat="server" ID="litDisclaimer2"></asp:Literal>
             </div>
  </div>
  
<div class="modal fade" id="partnersModal" tabindex="-1" role="dialog" aria-labelledby="marketingPartners" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="partnersModalLabel">Marketing Partners</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body" style="font-size: 11px;">
        Time 2 Read, Omni Research, jointpainreliefgroup.com, Loud Cloud Nine, The-Solar-Project.com, Debt.com, Diabetic Connect, Skyline Health Services, MyBundleSaver, Q3 Insurance Solutions, Automotive Services Center, Prestige Magazine Service, E Social Marketing, LLC, Assurance IQ Inc, Better Health Kare, Consumer Awareness Group, Debt Help Express, Nationwide Student Loan, All Finance 411, Credit Lair, TargetedCareer, Homebidz, Credit Fix, Best Rate Referrals, Alliance Home Security, Home Services Companies, Solar Advocacy Group, Solar Research Group, Solar Savings Center, Solar Source Group, Touchstone, Sweepstakes Entry Center, Mortgage Advisor, Verde Energy USA, Sealed Inc, Sprint, Instant Platinum Offers, Injury-Review, Direct Energy Services, Home Biz Search, Pillpack, Student Payment Relief, ReadyForMyQuote, Leadcreations.com, Affinity Marketing Partners, Anthem Tax, Precision Mortgage, Health Benefits One, Rick Case KIA, Tax Defense Network, Optima Tax, Community Tax, Intellectual Inc, Buyers Inc, TaxInvestigators.com, Tax Inc, Outsource Inc, Advertising Inc, Alumni Aid, NCAC, CSADVO, Champion Savings LLC., The Brace Doctor, DeletePoorCredit.com, Nationwide Resources, BankruptcyHelpers.org, Helping Hands Association, Royal Seas Cruises, Support First, Great American Readers, 247 Paydayloan Center, Drips.com, My Savings Alliance, Magazine Publisher Service, HomeBusinessFortune, All American Readers, Nature Coast Publications, Suretouchleads, Leadsrow, Vehicle Protection Center, Car Guardian, Ringchat, Lexington Law, Credit Fix, Health Right, Injury Attorneys Network, Wireless Medical Alert, Soleo Communications, Inc., Shop Your Way, Diabetic Discount club, Rxtome, Backpain Discount Club, Festive Health Backbrace Partners, Preferred Guest Resorts, Job Resource Center, JRC, Life Line Screening, OneTouch Direct, White & Twombly, P.A., Optimum Medical Supply, Easy Rest Beds, Help Advisors, Verus Healthcare, e-TeleQuote Insurance, Inc, Certainty Auto Protection, Roadstead Auto Protection, Brobiz LLC, Advantage Auto Protection, Everlast Home Energy Solutions, Active Home Marketing, Home Source Marketing, Eximia Health, Integrated Sales Solutions, LLC, Soleo Communications, Inc., Flatiron Media, Bankruptcy Attorneys Network, Pure Talk USA, Community Tax LLC, United Medicare Advisors, Senior Market Quotes, Open Market Quotes, Spring Insurance Solutions, TrueChoice Insurance Services, DR Media LLC, Education ProNetwork, Settlement Marketing Group, LLC, Paperback, LLC, Paperback Services, LLC, CDSC, LLC, GoCDSC, LLC, Spanish Leads, LLC, Tax Investigators LLC, Tax Inc, DebtRevision.com, AlumniAid.org, Advertising Inc, Callbids.com, Outsource LLC, Health Source Network, Help Advisors, Cege Media LLC, Oxford Tax Partners, EM Credit Corp, Advocator Group, Americas Debt Choice, Consolidated Credit Counseling Services, Lighthouse Finance Solution, Telcare, Fastflow, Forward Leap Marketing, Western Benefits Group, Viva Medical Supply, Arbor Medical, Modern Medical, Champion Medical, Universal Healthcare Group, Four Seasons Oxygen, Hogan P&O, Disability Advisor, CCCF (Credit Counseling), Solid Quote LLC, Edata By Design, Inc., SolidQuote LLC, Disability Help Center, Shore Marketing Group, TaxLeads.com, Fidelity Tax Relief, Silver Tax, Crisp Connections, Legacy Quote, Medicare Plus Card, Gerber Life Insurance, National Magazine Exchange, NationalTaxRelief, Global DS Group, LLC, Hear Better for Life
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
      </div>
    </div>
  </div>
</div>

  </fieldset>
                              </div>
      </div>
  </div>
 <table class="record_table">
    <tr>
        <td id="consent"><input type="checkbox" name="opt_in" id="opt_in" value="true" /></td>
        <td id="consentInfo">I Confirm that all of my information is accurate and consent to be contacted as provided above</td>
    </tr>
</table> 
</form>

       
      <input type="button" id="btnSubmit" class="submit submitForm submit-btn" value="SUBMIT" />
</div>
 
</div>
<script type="text/javascript">
   var arrayInfo = ['#txt_first_name', '#txt_last_name', '#txt_zip', '#txt_phone', '#hdn_dob', '#txt_address', '#disclaimer_1','#txt_email']; 
 $('.submitForm').click(function(){
   $("#hdn_dob").focus(); 
   $("#hdn_dob").blur(); 
    var popUpURL = '';
  	var moveon = validateFields(arrayInfo,true,popUpURL,'5de5511e-5228-42de-b76a-83288adb1beb');
});
    
   $(function(){
       prepFormContact();
       buildDOBDropdown('', '', '-1', '-1');

  });

</script>


    
    	<!-- Footer -->	
    <footer class="footer">
                  <!-- Skip Button -->
        <div class="row" id="skipButton" class="">
            <div class="col-md-12">
                <a onclick="clickCounter()" href="/api/offers"><h4 class="skip" style="color: #C6C4C1;float: right;padding-right: 20px;">Skip &nbsp;<i style="color:#C6C4C1;" class="fa fa-arrow-circle-right"></i></h4></a>
            </div>
        </div>    
         <div class="container text-center">
           Copyright &copy;
    		<span id="copyright">
        		<script>document.getElementById('copyright').appendChild(document.createTextNode(new Date().getFullYear()))</script>
    		</span>
   			Contest Review Guide, LLC 
          <a href="/Terms.aspx" target="_blank">Terms and Conditions</a> | <a href="/Privacy.aspx" target="_blank">Privacy Policy</a> | <a href="/Unsub.aspx" target="_blank">Unsubscribe</a>
        </div>  
    </footer>

    
    </asp:Content>