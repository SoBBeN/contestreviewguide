﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="NewsInfo.aspx.cs" Inherits="NewsInfo" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>
<%@ Register TagPrefix="fbc" TagName="FbComments" Src="~/UserControls/FbComments.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">var switchTo5x = true;</script>
    <script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({ publisher: "98c30075-93f7-4baa-9669-a5a1792d12a3", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
    <link href="/css/news.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            if (matchMedia('only screen and (min-width: 310px) and (max-width:500px)').matches) {
                //$('.ads_padding_middle').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- HMG - Body --><ins class="adsbygoogle" style="display:inline-block;width:98%;height:50px" data-ad-client="ca-pub-0634471641041185" data-ad-slot="9580178383" data-ad-format="auto"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});<\/script>');
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
<%--        <div class="ad6" style="text-align:center; padding:10px 0"></div>--%>
    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />
    <br />
    <div id="news">
        <asp:Literal runat="server" ID="litCoupons"></asp:Literal>
    </div>
<%--    <div class="ads_padding_middle"></div>--%>
    <br /> <br />
    <div id="poPrevNext">
        <div class="prev lnk"><div><a href="javascript:void(0);" runat="server" id="imgLnkPrev"><img runat="server" id="imgPrev" /></a></div><div><a href="javascript:void(0);" runat="server" id="lnkPrev" class="link">< previous</a></div></div>
        <div class="next lnk"><div><a href="javascript:void(0);" runat="server" id="lnkNext">next ></a></div><div><a href="javascript:void(0);" runat="server" id="imgLnkNext"><img runat="server" id="imgNext" /></a></div></div>
        <div style="clear:both;"></div>
        <div class="lnk all"><a target="_blank" href="/News">SEE ALL</a></div>
    </div>
    <div style="clear:both;"></div>
    <div id="fbCommentsPlaceholder">
        <fbc:FbComments id="fbComments1" runat="server" />
    </div>
</asp:Content>