﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class Photos : System.Web.UI.Page
{
    protected string dynamicStyles = string.Empty;

    protected void Page_Load(object sender, EventArgs e)
    {
        breadcrumbs1.AddLevel("Photos", null);

        int typeid;
        if (!int.TryParse(Convert.ToString(Request.QueryString["id"]), out typeid))
        {
            typeid = Convert.ToInt32(Page.RouteData.Values["id"]);
        }
        
        int photoid;
        if (!int.TryParse(Convert.ToString(Request.QueryString["photoid"]), out photoid))
        {
            if (!int.TryParse(Convert.ToString(Page.RouteData.Values["photoid"]), out photoid)) 
                photoid = -1;
        }

        ((ITmgMasterPage)Master).PageType = "article";
        LoadPhotos(typeid, photoid);

        if (photoid > 0)
            litID.Text += "<input type=\"hidden\" id=\"photoid\" value=\"" + photoid.ToString() + "\" />";
        else
            litID.Text += "<input type=\"hidden\" id=\"photoid\" value=\"\" />";
    }

    private void LoadPhotos(int typeid, int photoid)
    {
        StringBuilder sb = new StringBuilder();
        SqlDataReader dr = DB.DbFunctions.GetPhotosPaging(typeid, 0, 0);

        bool ogdone = false;

        if (dr != null && dr.HasRows)
        {
            int i = 0;
            bool adShown = false;

            while (dr.Read() && i++ < 9)
            {
                int id = Convert.ToInt32(dr["ID"]);

                string link = String.Format("<a href=\"javascript:void(0);\" onclick=\"OpenPhoto({0});\"", Convert.ToString(dr["ID"]));

                if (i == 1)
                {
                    if (DBNull.Value != dr["TypeDesc"])
                        litTitle.Text = Convert.ToString(dr["TypeDesc"]);

                    if (DBNull.Value != dr["SubTitle"])
                        litSubTitle.Text = Convert.ToString(dr["SubTitle"]);

                    sb.Append("<input type=\"hidden\" id=\"firstid\" value=\"" + Convert.ToString(dr["ID"]) + "\" />");

                    litID.Text = "<input type=\"hidden\" id=\"typeid\" value=\"" + typeid.ToString() + "\" />"
                               + "<input type=\"hidden\" id=\"titleurl\" value=\"" + Functions.StrToURL(Convert.ToString(dr["TypeDesc"])) + "\" />"
                               + "<input type=\"hidden\" id=\"titletext\" value=\"" + Functions.RemoveHtml(Convert.ToString(dr["Text"])) + "\" />";

                    litTitleIn.Text = Convert.ToString(dr["TypeDesc"]);
                    breadcrumbs1.AddLevel(Convert.ToString(dr["TypeDesc"]), "/Photos/" + typeid.ToString() + "/" + Convert.ToString(dr["TypeDesc"]));
                }

                if ((i == 1 && photoid == -1) || id == photoid)
                {
                    ((ITmgMasterPage)Master).PageLogo = "images/" + Convert.ToString(dr["ImageFilename"]);
                    ((ITmgMasterPage)Master).PageDescription = Functions.RemoveHtml(Convert.ToString(dr["Text"]));
                    ((ITmgMasterPage)Master).PageTitle = Convert.ToString(dr["TypeDesc"]);
                    ((ITmgMasterPage)Master).PageURL = "Photos/" + typeid.ToString() + "/" + Functions.StrToURL(Convert.ToString(dr["TypeDesc"])) + "/" + photoid.ToString();
                    ogdone = true;
                }

                sb.Append("<div class=\"photo\">");
                sb.Append("<div onclick=\"OpenPhoto(").Append(dr["ID"]).Append(")\" style=\"background-image:url('" + System.Configuration.ConfigurationManager.AppSettings["baseimagesurl"] + "images/").Append(Convert.ToString(dr["ImageFilename"]).Replace("'","\\'")).Append("');\" /></div>\n");
                sb.Append("</div>");

                //if (i % 3 == 0)
                //{
                //    if (Request.Browser.IsMobileDevice && !adShown)
                //    {
                //        sb.Append("<div class=\"ad\">");
                //        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- HMG - Body --><ins class=\"adsbygoogle\" style=\"display:inline-block;width:98%;height:50px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"9580178383\" data-ad-format=\"auto\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
                //        sb.Append("</div><div style=\"clear:both;\">");

                //        adShown = true;
                //    }
                //}
            }

            //if (i < 3)
            //{
            //    if (Request.Browser.IsMobileDevice)
            //    {
            //        sb.Append("<div class=\"ad\">");
            //        sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- HMG - Body --><ins class=\"adsbygoogle\" style=\"display:inline-block;width:98%;height:50px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"9580178383\" data-ad-format=\"auto\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
            //        sb.Append("</div><div style=\"clear:both;\"></div>");
            //    }
            //}

            litPhotos.Text = sb.ToString();
        }
        dr.Close();
     dr.Dispose();

        if (!ogdone && photoid > 0)
        {
            dr = DB.DbFunctions.GetPhoto(photoid, 0, typeid);
            if (dr != null && dr.HasRows)
            {
                if (dr.Read())
                {
                    ((ITmgMasterPage)Master).PageLogo = "images/" + Convert.ToString(dr["ImageFilename"]);
                    ((ITmgMasterPage)Master).PageDescription = Convert.ToString(dr["Text"]);
                    ((ITmgMasterPage)Master).PageTitle = Convert.ToString(dr["TypeDesc"]);
                    ((ITmgMasterPage)Master).PageURL = "Photos/" + typeid.ToString() + "/" + Convert.ToString(dr["TypeDesc"]);
                }
            }
        }
    }
}