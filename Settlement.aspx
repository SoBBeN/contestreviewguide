﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="Settlement.aspx.cs" Inherits="Settlement" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>
<%@ Register TagPrefix="fbc" TagName="FbComments" Src="~/UserControls/FbComments.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="/css/Settlements.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            if (matchMedia('only screen and (min-width: 310px) and (max-width:500px)').matches) {
                $('.ads_padding_middle').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- HMG - Body --><ins class="adsbygoogle" style="display:inline-block;width:98%;height:50px" data-ad-client="ca-pub-0634471641041185" data-ad-slot="9580178383" data-ad-format="auto"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});<\/script>');
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />
    <div id="topcoupon">
        <div class="title"><asp:Literal runat="server" ID="litTitle" /></div>
        <div class="date"><asp:Literal runat="server" ID="litDate" /></div>
        <div class="body">
            <div class="section">
                <div class="image">
                    <img runat="server" id="imgTopCoupon" />
                </div>
                <div class="text_image"><asp:Literal runat="server" ID="litText" /></div>
            </div>
            <asp:Literal runat="server" ID="litEligible"></asp:Literal>
            <asp:Literal runat="server" ID="litPotentialAward"></asp:Literal>
            <asp:Literal runat="server" ID="litProofPurchase"></asp:Literal>
            <asp:Literal runat="server" ID="litClaimFormDeadline"></asp:Literal>
            <div class="section">
                <div class="menu">
                    Claim Form
                </div>
                <div class="text button">
                    <a href="" runat="server" id="lnkBtn" target="_blank">Submit Claim</a>
                </div>
            </div>
            <div class="clear"></div>
<%--            <div class="ads_padding_middle"></div>--%>
         
            <div id="socialst">
                <div class='st_facebook_hcount item' displayText='Facebook'></div>
                <div class='st_twitter_hcount item' displayText='Tweet'></div>
                <div class='st_pinterest_hcount item' displayText='Pinterest'></div>
                <div class='st_email_hcount item' displayText='Email'></div>
            </div>
        </div>
    </div>
    <br />
    <div id="poPrevNext">
        <div class="prev lnk"><div><a href="javascript:void(0);" runat="server" id="imgLnkPrev"><img runat="server" id="imgPrev" /></a></div><div><a href="javascript:void(0);" runat="server" id="lnkPrev" class="link">< previous</a></div></div>
        <div class="next lnk"><div><a href="javascript:void(0);" runat="server" id="lnkNext">next ></a></div><div><a href="javascript:void(0);" runat="server" id="imgLnkNext"><img runat="server" id="imgNext" /></a></div></div>
        <div style="clear:both;"></div>
        <div class="lnk all"><a target="_blank" href="/Rebates">SEE ALL</a></div>
    </div>
    <div style="clear:both;"></div>
    <div id="fbCommentsPlaceholder">
        <fbc:FbComments id="fbComments1" runat="server" />
    </div>
    <br /><br />
</asp:Content>
