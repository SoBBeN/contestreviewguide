﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DailyNumberSign.aspx.cs" Inherits="DailyNumberSign" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <link href="/css/horoscope.css" rel="stylesheet" />
    <link rel="stylesheet" href="/css/odometer-theme.css" />
    <script src="/js/odometer.min.js"></script>
    <script src="/js/chance.min.js"></script>
    <script>
        $(document).ready(function () {
            var uniques = chance.unique(chance.natural, 7, { min: 1, max: 59 });

            setTimeout(function () {
                $.each(uniques, function (index, value) {
                    $('.odometer' + (index + 1)).html(value);
                });
            }, 100);

            if (matchMedia('only screen and (min-width: 310px) and (max-width:500px)').matches) {
                $('.ads_padding_middle').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- AG content - Body --><ins class="adsbygoogle"style="display:inline-block;width:98%;height:50px"data-ad-client="ca-pub-0634471641041185"data-ad-slot="1654153183"></ins><script>        (adsbygoogle = window.adsbygoogle || []).push({});<\/script>');
            }
        });
    </script>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />

    <div id="content">
        <h1><asp:Literal runat="server" ID="litTitle">Daily Lucky Numbers</asp:Literal></h1>
        <hr class="blueline" />
        <div class="sign">
            <div class="sign_img">
                <img id="imgSign" runat="server" /></div>
            <div class="daily_number">
                <div class="odometer odometer1">00</div> - 
                <div class="odometer odometer2">00</div> - 
                <div class="odometer odometer3">00</div> - 
                <div class="odometer odometer4">00</div> - 
                <div class="odometer odometer5">00</div> - 
                <div class="odometer odometer6">00</div> - 
                <div class="odometer odometer7">00</div>
            </div>
        </div>
  <%--      <div class="ads_padding_middle">&nbsp;</div>--%>
        <div class="daily_other">
            View Daily Lucky Numbers for Other Signs
        </div>
        <hr class="blueline" />
        <div class="signs daily">
            <div>
                <a href="/DailyNumbers/Aries"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/aries_sign.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Taurus"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/taurus_sign.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Gemini"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/gemini_sign.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Cancer"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/cancer_sign.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Leo"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/leo_sign.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Virgo"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/virgo_sign.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Libra"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/libra_sign.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Scorpio"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/scorpio_sign.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Sagittarius"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/sagittarius_sign.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Capricorn"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/capricorn_sign.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Aquarius"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/aquarius_sign.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Pisces"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/pisces_sign.png" /></a></div>
        </div>
    </div>
</asp:Content>
