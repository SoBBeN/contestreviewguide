﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class PointsOffersList : System.Web.UI.Page
{
    private const int NB_PER_PAGE = 6;

    protected void Page_Load(object sender, EventArgs e)
    {
		breadcrumbs1.AddLevel("Daily Deals", "/DailyDeals/");

		SqlDataReader dr;

		dr = DB.DbFunctions.GetPointsOffersList(-1);

		if (dr != null)
		{
			if (dr.HasRows)
			{
				int start = 0;
				if (int.TryParse(Request.QueryString["s"], out start) && start >= NB_PER_PAGE)
				{
					lnkPrev.HRef = "/DailyDeals/?s=" + (start - NB_PER_PAGE).ToString();
					lnkPrev.Visible = true;
				}

				lnkNext.HRef = "/DailyDeals/?s=" + (start + NB_PER_PAGE).ToString();
				while (start-- > 0 && dr.Read()) ;

				int i = 0;
				StringBuilder sb = new StringBuilder();
				while (dr.Read() && i++ < NB_PER_PAGE)
				{
					string link = "<a target='_blank' href='" + Convert.ToString(dr["URL"]) + "'>";

					sb.Append("<div class=\"coupon\">");
					sb.AppendFormat("<div class=\"title\">{0}</div>", Functions.ConvertToString(dr["Title"]));
					sb.AppendFormat("<div class=\"img\"><a href=\"/DailyDeals/" + dr["ID"] + "/" + Functions.StrToURL(Functions.ConvertToString(dr["Title"])) + "/\"><img src=\"{0}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"] + Functions.ConvertToString(dr["Thumbnail"]));
					sb.Append("<div class=\"details\">");
					if (dr["DateExpire"] != DBNull.Value)
					{
						DateTime expireDate = Convert.ToDateTime(dr["DateExpire"]);
						sb.Append("<div class=\"expire\"><span style=\"font-style:italic\">Expires on</span> " + Functions.UppercaseFirst(expireDate.ToString("MMMM d")) + Functions.GetDateSuffix(expireDate) + ", " + expireDate.ToString("yyyy") + "</div>");
					}
					sb.AppendFormat("<div class=\"description\">{0}</div>", Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 300));
					sb.AppendFormat("<div class=\"button\">{0}<img src=\"/images/grabthisdeal.png\" /></a></div>", link);

					sb.Append("</div>");
					sb.Append("</div><div style=\"clear:both;\"></div><hr />");
                    if (i > 0)
                    {
                        if (i == 2) { sb.Append("<div class=\"new_ads\"><a target=\"_blank\" href=\"http://youropiniontrk.com/?a=1288&c=84116&s1=OMG\"><img src=\"" + System.Configuration.ConfigurationManager.AppSettings["AssetPath"] + "/images/18617.jpg\" /></a></div><hr />"); }
                        else if (i == 4) { sb.Append("<div class=\"new_ads\"><a target=\"_blank\" href=\"http://youropiniontrk.com/?a=1288&c=85806&s1=OMG\"><img src=\"" + System.Configuration.ConfigurationManager.AppSettings["AssetPath"] + "/images/62846.jpg\" /></a></div><hr />"); }
                        else if (i == 6) { sb.Append("<div class=\"new_ads\"><a target=\"_blank\" href=\"http://youropiniontrk.com/?a=1288&c=85895&s1=OMG\"><img src=\"" + System.Configuration.ConfigurationManager.AppSettings["AssetPath"] + "/images/81490.jpg\" /></a></div><hr />"); }
                    }
                    litCoupons.Text = "<hr />" + sb.ToString();
				}
				if (i > NB_PER_PAGE)
					lnkNext.Visible = true;
			}
			dr.Close();
			dr.Dispose();
		}


		//breadcrumbs1.AddLevel("Daily Deals", "/Products/");

		//SqlDataReader dr;

		//dr = DB.DbFunctions.GetPointsOffersList(-1);

		//if (dr != null)
		//{
		//    if (dr.HasRows)
		//    {
		//        int start = 0;
		//        if (int.TryParse(Request.QueryString["s"], out start) && start >= NB_PER_PAGE)
		//        {
		//            lnkPrev.HRef = "/Products/?s=" + (start - NB_PER_PAGE).ToString();
		//            lnkPrev.Visible = true;
		//        }

		//        lnkNext.HRef = "/Products/?s=" + (start + NB_PER_PAGE).ToString();
		//        while (start-- > 0 && dr.Read());

		//        int i = 0;
		//        StringBuilder sb = new StringBuilder();
		//        bool first = true;
		//        while (dr.Read() && i++ < NB_PER_PAGE)
		//        {
		//            string link = "<a href=\"/Products/" + dr["ID"] + "/" + Functions.StrToURL(Functions.ConvertToString(dr["Title"])) + "/\">";

		//            sb.Append("<div class=\"coupon\">");
		//            sb.AppendFormat("<div class=\"title\">{0}{1}</a></div>", link, Functions.ConvertToString(dr["Title"]));
		//            sb.AppendFormat("<div class=\"img\"><a href=\"/Products/" + dr["ID"] + "/" + Functions.StrToURL(Functions.ConvertToString(dr["Title"])) + "/\"><img src=\"{0}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"] + Functions.ConvertToString(dr["Thumbnail"]));
		//            sb.Append("<div class=\"details\">");
		//            if (dr["DateExpire"] != DBNull.Value)
		//            {
		//                DateTime expireDate = Convert.ToDateTime(dr["DateExpire"]);
		//                sb.Append("<div class=\"expire\"><span style=\"font-style:italic\">Expires on</span> " + Functions.UppercaseFirst(expireDate.ToString("MMMM d")) + Functions.GetDateSuffix(expireDate) + ", " + expireDate.ToString("yyyy") + "</div>");
		//            }
		//            sb.AppendFormat("<div class=\"description\">{0}</div>", Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 300));
		//            sb.AppendFormat("<div class=\"button\">{0}<img src=\"/images/learnmore.png\" /></a></div>", link);

		//            sb.Append("</div>");
		//            sb.Append("</div><div style=\"clear:both;\"></div><hr />");

		//            if (i % 3 == 0)
		//            {
		//                if (Request.Browser.IsMobileDevice)
		//                {
		//                    sb.Append("<div class=\"ad\">");
		//                    sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- HMG - Body --><ins class=\"adsbygoogle\" style=\"display:inline-block;width:98%;height:50px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"9580178383\" data-ad-format=\"auto\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
		//                    sb.Append("</div><div style=\"clear:both;\"></div><hr />");
		//                }
		//            }
		//        }

		//        if (i < 3)
		//        {
		//            if (Request.Browser.IsMobileDevice)
		//            {
		//                sb.Append("<div class=\"ad\">");
		//                sb.Append("<script async src=\"//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js\"></script><!-- HMG - Body --><ins class=\"adsbygoogle\" style=\"display:inline-block;width:98%;height:50px\" data-ad-client=\"ca-pub-0634471641041185\" data-ad-slot=\"9580178383\" data-ad-format=\"auto\"></ins><script>(adsbygoogle = window.adsbygoogle || []).push({});</script>");
		//                sb.Append("</div><div style=\"clear:both;\"></div><hr />");
		//            }
		//        }

		//        litCoupons.Text = sb.ToString();
		//        if (i > NB_PER_PAGE)
		//            lnkNext.Visible = true;
		//    }
		//    dr.Close();
		//    dr.Dispose();
		//}
	}
}