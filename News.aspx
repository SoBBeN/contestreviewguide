﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="News.aspx.cs" Inherits="News" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">
    <script type="text/javascript">var switchTo5x = true;</script>
    <script type="text/javascript" src="https://ws.sharethis.com/button/buttons.js"></script>
    <script type="text/javascript">stLight.options({ publisher: "98c30075-93f7-4baa-9669-a5a1792d12a3", doNotHash: false, doNotCopy: false, hashAddressBar: false });</script>
    <link href="/css/news.css" rel="stylesheet" />
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />
    
    <div id="content">
        <h1>Contest News</h1>
        <div style="text-align:left; font-size:12px; color:black; font-weight:bold; line-height:14px;">
            <br />
            <hr class="blueline" />
        </div>
        <div id="news">
            <asp:Literal runat="server" ID="litCoupons"></asp:Literal>
        </div>
        <div id="poPrevNext">
            <div class="prev"><a runat="server" id="lnkPrev" class="lnk" visible="false">< previous</a></div>
            <div class="next"><a runat="server" id="lnkNext" class="lnk" visible="false">next ></a></div>
            <div style="clear:both;"></div>
        </div>
    </div>
</asp:Content>