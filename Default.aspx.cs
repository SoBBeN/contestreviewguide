﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Text;
using System.Web;
using System.Web.Services;
using System.Web.UI;
using System.Web.UI.HtmlControls;
using System.Web.UI.WebControls;


public partial class _Default : System.Web.UI.Page
{
    private const int NB_PER_PAGE = 1;
    protected string dynamicStyles = string.Empty;
    protected string fbTalkingPointLink;
    protected string litFbDayJS;

    protected void Page_Load(object sender, EventArgs e)
    {
        if (!Page.IsPostBack)
        {
            ((ITmgMasterPage)Master).PageDescription = System.Configuration.ConfigurationManager.AppSettings["description"];
            //((ITmgMasterPage)Master).PageLogo = System.Configuration.ConfigurationManager.AppSettings["logo"];

            int id;
            id = -2147483648;

            SqlDataReader dr;
            dr = DB.DbFunctions.GetLifestyle(id);

            if (dr.HasRows)
            {
                dr.NextResult();
                int i = 0;
                StringBuilder sb = new StringBuilder();
                while (dr.Read() && i++ < NB_PER_PAGE)
                {
                    string url = "Lifestyle/" + dr["ID"] + "/" + Functions.StrToURL(dr["CategoryDescription"]) + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                    string link = String.Format("<a href=\"/{0}\">", url);
                    string imageurl = System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"] + Convert.ToString(dr["Thumbnail"]);
                    string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["DescriptionTop"]) + " " + Convert.ToString(dr["Description"])), 150).Replace("\"", " ").Trim();
                    string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Title"])), 300).Replace("\"", " ").Trim();

                    sb.Append("<div class=\"lifestyle\">");
                    sb.Append("<div class=\"img\">").Append(link).AppendFormat("<img src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimageslifestyleurl"], Functions.ConvertToString(dr["Thumbnail"]));
                    sb.Append("<div class=\"description\">");
                    sb.AppendFormat("<div class=\"title\">{1}{0}</a></div>", title, link);
                    /*sb.Append("<hr id=\"redline\" />");*/
                    sb.Append(description);
                    sb.Append("</div>");
                    sb.Append("</div>");
                }

                //litArticles.Text = sb.ToString();
            }

            id = -1;

            dr = DB.DbFunctions.GetNews(id);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    int i = 0;
                    StringBuilder sb = new StringBuilder();
                    bool first = true;
                    while (dr.Read() && i++ < NB_PER_PAGE)
                    {
                        string newsurl = "/News/" + dr["ID"] + "/" + Functions.StrToURL(Convert.ToString(dr["Title"])) + "/";
                        string imageurl = System.Configuration.ConfigurationManager.AppSettings["baseimagesnewsurl"] + Convert.ToString(dr["Thumbnail"]);
                        string description = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 220).Replace("\"", " ").Trim();
                        string title = Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Title"])), 220).Replace("\"", " ").Trim();

                        sb.Append("<div class=\"new\">");
                        sb.AppendFormat("<div class=\"img\"><a href=\"{3}\"><img alt=\"{2}\" src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagesnewsurl"], Functions.ConvertToString(dr["Thumbnail"]), Convert.ToString(dr["Title"]), newsurl);
                        sb.Append("<div class=\"description\">");
                        sb.AppendFormat("<h2 class=\"title\"><a href=\"{0}\">{1}</a></a></h2>", newsurl, Functions.ConvertToString(dr["Title"]));
                        sb.Append(Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 300));
                        sb.Append("</div>");
                        sb.Append("</div>");

                        litNews.Text = sb.ToString();
                    }
                }
                dr.Close();
                dr.Dispose();
            }
            
                  
                    //StringBuilder sbJ = new StringBuilder();
                    //    string linkJ = "<a href='http://www.theamericancareerguide.com/hosting/staticpages/ACG_Jobs.aspx?title=&location=Fort+Lee%2c+New+Jersey&page=5'>";
                    //    string textJ = "<p><span style=\"line-height:2\"><span style=\"font-size:14px\"><span style=\"font-family:arial,helvetica,sans-serif\">Did you know that computer peripherals like printers and scanners continue to use energy, even after you turn them off?</span></span></span></p><p><span style=\"line-height:2\"><span style=\"font-size:14px\"><span style=\"font-family:arial,helvetica,sans-serif\">The Smart Strip helps you easily save time and money on your energy bills while giving your computers superior surge protection.&nbsp;While your peripherals are still plugged into the outlets, they continue to suck energy even while it is turned off. So you are paying for that energy. This &#39;vampire&#39; current (known as idle current) can be tamed with the&nbsp;intelligent circuitry of the Smart Strip power strip, from Bits Ltd. More than just a power strip the Smart Strip actually senses how much power your computer peripherals use. And when the Smart Strip senses that you&#39;ve turned your computer off, it automatically shuts off your peripherals, too, preventing them from drawing this vampire current.</span></span></span></p><p><span style=\"line-height:2\"><span style=\"font-size:14px\"><span style=\"font-family:arial,helvetica,sans-serif\">Studies have shown that the Smart Strip Power Strip pays for itself in as little as six weeks! Best of all?&nbsp;It&#39;s as easy as 1-2-3 and you can eliminate the hassle of having to turn computer peripherals on or off again!</span></span></span></p>";
                    //    sbJ.Append("<div class=\"Job\">");
                    //    sbJ.AppendFormat("<div class=\"img\">{3}<img alt=\"{2}\" src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["AssetPath"], "/images/local_jobs.png", "The Good News", linkJ);
                    //    sbJ.Append("<div class=\"description\">");
                    //    sbJ.AppendFormat("<h2 class=\"title\">{0}{1}</a></a></h2>", linkJ,"The Good News For Local Job Markets");
                    //    sbJ.Append(Functions.ShortenText(Functions.RemoveHtml(textJ), 300));
                    //    sbJ.Append("</div>");
                    //    sbJ.Append("</div>");
                    

                    //litJobs.Text = sbJ.ToString();


            dr = DB.DbFunctions.GetPointsOffersList(id);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    int i = 0;
                    StringBuilder sb = new StringBuilder();
                    while (dr.Read() && i++ < NB_PER_PAGE)
                    {
                        string link = "<a href=\"/Products/" + dr["ID"] + "/" + Functions.StrToURL(Functions.ConvertToString(dr["Title"])) + "/\">";

                        sb.Append("<div class=\"new\">");
                        sb.AppendFormat("<div class=\"img\">{3}<img alt=\"{2}\" src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagescouponsurl"], Functions.ConvertToString(dr["Thumbnail"]), Convert.ToString(dr["Title"]), link);
                        sb.Append("<div class=\"description\">");
                        sb.AppendFormat("<h2 class=\"title\">{0}{1}</a></a></h2>", link, Functions.ConvertToString(dr["Title"]));
                        sb.Append(Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 300));
                        sb.Append("</div>");
                        sb.Append("</div>");
                    }

                    //litProducts.Text = sb.ToString();
                }
                dr.Close();
                dr.Dispose();
            }
            
            dr = DB.DbFunctions.GetSweepContestsList(id, true);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    int i = 0;
                    StringBuilder sb = new StringBuilder();
                    while (dr.Read() && i++ < NB_PER_PAGE)
                    {
                        string link = "<a href=\"/Samples/" + dr["ID"] + "/" + Functions.StrToURL(Functions.ConvertToString(dr["Title"])) + "/\">";

                        sb.Append("<div class=\"new\">");
                        sb.AppendFormat("<div class=\"img\">{3}<img alt=\"{2}\" src=\"{0}{1}\"></a></div>", System.Configuration.ConfigurationManager.AppSettings["baseimagessweepcontesturl"], Functions.ConvertToString(dr["Thumbnail"]), Convert.ToString(dr["Title"]), link);
                        sb.Append("<div class=\"description\">");
                        sb.AppendFormat("<h2 class=\"title\">{0}{1}</a></a></h2>", link, Functions.ConvertToString(dr["Title"]));
                        sb.Append(Functions.ShortenText(Functions.RemoveHtml(Convert.ToString(dr["Text"])), 300));
                        sb.Append("</div>");
                        sb.Append("</div>");
                    }

                   // litSamples.Text = sb.ToString();
                }
                dr.Close();
                dr.Dispose();
            }

            dr = DB.DbFunctions.GetContent(204);

            if (dr != null)
            {
                if (dr.HasRows)
                {
                    if (dr.Read())
                    {
                        litDescriptionTop.Text = Functions.ConvertToString(dr["DescriptionTop"]);
                        litDescriptionBtm.Text = Functions.ConvertToString(dr["DescriptionBtm"]);

                        if (dr["ImageFilename"] != null && Convert.ToString(dr["ImageFilename"]).Length > 0)
                        {
                            if (Functions.ConvertToString(dr["ImageLink"]).Length > 0)
                                litImage.Text = "<a href=\"" + Functions.ConvertToString(dr["ImageLink"]) + "\" target=\"_blank\">";
                            else
                                litImage.Text = "<a>";

                            litImage.Text += "<img src=\"" + System.Configuration.ConfigurationManager.AppSettings["baseimagescontentsurl"] + Functions.ConvertToString(dr["ImageFilename"]) + "\" id=\"imgMain\" class=\"img\" />";
                            litImage.Text += "</a>";
                        }
                        else
                            litImage.Visible = false;
                    }
                }

                dr.Close();
                dr.Dispose();
            }

            if (Request.QueryString["lid"] != null && Request.QueryString["lid"] != "")
            {
                dr = DB.DbFunctions.GetLeadData(int.Parse(Request.QueryString["lid"].ToString()));

                if (dr != null)
                {
                    if (dr.HasRows)
                    {
                        while (dr.Read())
                        {
                            //var forwardedFor = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
                            //string IpAddress = String.IsNullOrWhiteSpace(forwardedFor) ? Request.ServerVariables["REMOTE_ADDR"] : forwardedFor.Split(',').Select(s => s.Trim()).First();
                            string IpAddress = Request.UserHostAddress;
                            //DB.DbFunctions.PostToAllInbox("716", dr["SessionID"].ToString(), dr["emailaddress"].ToString(), dr["firstname"].ToString(), IpAddress, dr["lastname"].ToString(), dr["homephoneno"].ToString(), dr["referid"].ToString());
                        }
                    }
                }

                dr.Close();
                dr.Dispose();
            }
        }
    }
}