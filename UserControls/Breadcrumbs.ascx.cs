﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

public partial class UserControls_Breadcrumbs : System.Web.UI.UserControl
{
    protected string dataSeparator = ">";
    protected Dictionary<string, string> levels = new Dictionary<string, string>();

    public string Separator
    {
        get { return dataSeparator; }
        set { dataSeparator = value; }
    }

    public void AddLevel(string name, string link)
    {
        levels.Add(name, link);
    }

    protected void Page_PreRender(object sender, EventArgs e)
    {
        StringBuilder sb = new StringBuilder();
        int i = 2;
        foreach (var level in levels)
        {
            sb.Append("&nbsp;").Append(dataSeparator).Append("&nbsp;");
            if (level.Value != null)
            {
                sb.Append("<a href=\"").Append(level.Value).Append("\" id=\"bcLevel").Append(i++).Append("\">");
                sb.Append(level.Key).Append("</a>");
            }
            else
            {
                i++;
                sb.Append(level.Key);
            }
	    }
        litBreadcrumbs.Text = sb.ToString();
    }
}