﻿<%@ Page Language="C#" MasterPageFile="~/MasterPage.master" AutoEventWireup="true" CodeFile="DailyNumbers.aspx.cs" Inherits="DailyNumbers" %>
<%@ Register TagPrefix="bc" TagName="Breadcrumbs" Src="~/UserControls/Breadcrumbs.ascx" %>

<asp:Content ID="Content1" ContentPlaceHolderID="head" Runat="Server">

    <link href="/css/horoscope.css" rel="stylesheet" />

    <script type="text/javascript">
        $(document).ready(function () {
            if (matchMedia('only screen and (min-width: 310px) and (max-width:500px)').matches) {
                //$('.ads_padding_middle').append('<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js"><\/script><!-- AG content - Body --><ins class="adsbygoogle"style="display:inline-block;width:98%;height:50px"data-ad-client="ca-pub-0634471641041185"data-ad-slot="1654153183"></ins><script>        (adsbygoogle = window.adsbygoogle || []).push({});<\/script>');
                //$('.ads_padding_middle').css("clear", "both");
                //$('.ads_padding_middle').css("width", "100%");
                //$('.ads_padding_middle').css("margin", "unset");
            }
            else
            {
                //$('.ads_padding_middle').css("display", "none");
            }
        });
    </script>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="ContentPlaceHolderMain" Runat="Server">
    <bc:Breadcrumbs id="breadcrumbs1" runat="server" />

    <div id="content">
        <div class="ckedit"><asp:Literal runat="server" ID="litDescriptionTop"></asp:Literal></div>
        <asp:Literal runat="server" ID="litImage"></asp:Literal>
        <div class="ckedit"><asp:Literal runat="server" ID="litDescriptionBtm"></asp:Literal></div>
        <h1><asp:Literal runat="server" ID="litTitle">Daily Lucky Numbers</asp:Literal></h1>
        <hr class="blueline" />
        <div class="zodiac_traits">Your daily lucky numbers for your games</div>
        <div class="signs">
            <div>
                <a href="/DailyNumbers/Aries"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/aries_number.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Taurus"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/taurus_number.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Gemini"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/gemini_number.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Cancer"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/cancer_number.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Leo"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/leo_number.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Virgo"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/virgo_number.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Libra"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/libra_number.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Scorpio"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/scorpio_number.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Sagittarius"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/sagittarius_number.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Capricorn"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/capricorn_number.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Aquarius"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/aquarius_number.png" /></a></div>
            <div>
                <a href="/DailyNumbers/Pisces"><img src="<%=System.Configuration.ConfigurationManager.AppSettings["AssetPath"] %>/images/pisces_number.png" /></a></div>
        </div>
        <div class="zodiac_meaning">Click on your sign to consult your lucky numbers</div>
    </div>
</asp:Content>

<asp:Content ID="Content3" ContentPlaceHolderID="OutsideForm" Runat="Server">

</asp:Content>

